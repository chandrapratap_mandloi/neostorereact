import React, { Component } from 'react'
// import ReactDOM from 'react-dom';
import {DebounceInput} from 'react-debounce-input';
import BaseURL from './Baseurl'
import axios from 'axios'
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';
const customStyles={
    maindiv:{
        paddingTop:'5%',
        paddingLeft:'5%'
    },
    inputfield:{
        width:380, 
        backgroundColor:'white',
        height:40,
        border:'none'
    },
    subdiv:{
        width: 406,
        alignItems: 'center',
        border: '1px solid',
        display: 'flex'
    },
    allproductbtn: {
        backgroundColor: '#f4f4f4',
        color: '#444',
        cursor: 'pointer',
        padding: 18,
        width: '40%',
        textAlign: 'left',
        border: 'none'
    },
    ulStyle:{
        margin:0,
        listStyleType: 'none',
        display:'flex',
        flexDirection:'column',
        width:'100%'
    }
}
class Search extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            value: '',
            products:[]
        }
    }
    handleSearch = (event) => {
        console.log(event.target.value)
        // suggestion
        // this.setState({
        //     value: event.target.value
        // })
        this.setState({
            value: event.target.value
        }, () =>
            axios.get(`${BaseURL}/suggestion?text=${this.state.value}`)
                .then(response => {
                    console.log(response.data)
                    this.setState({
                        products:response.data.product
                    })
                })
                .catch(error => { console.log(error, 'error') })
        )
    }
    // renderlistItem(){
    //     const {products} = this.state
    //     return produ(

    //     )
    // }
    render() {
        const {products} = this.state
        return (
            <div style={customStyles.maindiv}>
                <div style={customStyles.subdiv}>
                <DebounceInput
                    minLength={2}
                    debounceTimeout={300}
                    style={customStyles.inputfield}
                    // onChange={event => this.setState({value: event.target.value})} 
                    onChange={this.handleSearch}
                />
                <SearchIcon/>
                </div>
                <ul style={customStyles.ulStyle}>
                    {products.map((step, index) =>  
                    <Button style={customStyles.allproductbtn}>
                        <li>{step.product_name}</li>
                    </Button>
                        
                    )}
                    {/* <li>Coffee</li>
                    <li>Tea</li>
                    <li>Milk</li> */}
                </ul>
                <p>Value: {this.state.value}</p>
            </div>
        )
    }
}

export default Search
