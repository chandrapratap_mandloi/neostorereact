import React, { Component } from 'react'
import Modal from 'react-modal';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
// import CloseIcon from '@material-ui/icons/Close';
import Rating from '@material-ui/lab/Rating';
import axios from 'axios';
import Decrypt from './Userdata'
import BaseURL from './Baseurl'
const customStyles = {
  
  button:{
    backgroundColor:'#3f51b5',
    color:'white',
    marginRight:'5%'
},
gridstyle:{
    textAlign:'center'
},
subimg:{
    height:100,
    width:150,
    marginRight:'2%',
   
},
font:{
    fontSize:30,
    textAlign:'center'
}
};
const Modalstyle={
    content: {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)',
      width                 : '30%'
    },
}
class RateProduct extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      modalIsOpen: this.props.isVisible,
      rating:0,
      userDetails: Decrypt().user,
      productdesc: props.productdesc[0]
    }
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  openModal() {
    this.setState({ modalIsOpen: true });
  }

  afterOpenModal() {
    // this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.props.cbClose(false)
  }
  
  handleClientChange = (event) => {
    this.setState({
      client: event.target.value
    })
  }
  handleissueChange = (event) => {
    this.setState({
      issues: event.target.value
    })
  }
  handlePriorityChange = (event) => {
    this.setState({
      help: event.target.value
    })
  }
  componentDidMount() {
    window.scrollTo(0, 0);
    Modal.setAppElement('body');
  }


  handleSubmit = (event) => {
    console.log(this.state.productdesc._id)
    const { userDetails, productdesc } = this.state
    event.preventDefault()
    const config = {
      headers: {
          'Authorization':`${userDetails.token}`
      }
    }
    const data = {
      product_id:productdesc._id,
      rating:this.state.rating
    }
  console.log('address')
      axios.post(`${BaseURL}/rating`,data,config)
      .then(response =>{
          if(response.data.success)
          {
              console.log(response.data)
              this.props.cbClose(false)
          }
      })
      .catch(error => {console.log(error,'error')})
    
  }
    handleChange = (event) => {
        this.setState({
            adminComment: event.target.value
        })
    }
    handleRatingChange= (event)=> {
        console.log(event.target.value)
        this.setState({
            rating:event.target.value
        })
    }
  render() {
    const { img } = this.props
    return (
      <Modal
        isOpen={this.props.isVisible}
        onAfterOpen={this.afterOpenModal}
        onRequestClose={this.closeModal}
        style={Modalstyle}
        contentLabel="Example Modal"
      >
       
        {/* <CloseIcon onClick={this.closeModal} /> */}
            <Grid container spacing={3}>
                <Grid style={customStyles.font} item xs={12}> Rate Product </Grid>
                <hr/>
                <Grid style={customStyles.gridstyle} item xs={12}>
                    <img style={customStyles.subimg} src={`${BaseURL}/${img[0]}`} alt='Product' />
                </Grid>
                <Grid style={customStyles.gridstyle}  item xs={12}>
                    <Rating onClick={this.handleRatingChange} name="half-rating" value={this.state.rating} precision={0.5} />
                </Grid>              
                <Grid style={customStyles.gridstyle}  item xs={12}>
                    <Button onClick={this.handleSubmit} variant="contained" style={customStyles.button} >
                        Rate Product
                    </Button>
                </Grid>
                </Grid>
      </Modal>

    )
  }
}

export default RateProduct

