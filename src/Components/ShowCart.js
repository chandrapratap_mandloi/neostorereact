import React, { Component } from 'react'
import Decrypt from './Userdata'
import axios from 'axios'
// import BasURL from './Baseurl'
import Grid from '@material-ui/core/Grid';
import BaseURL from './Baseurl'
import Button from '@material-ui/core/Button';
import '../CSS/user.css'
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircleOutline from '@material-ui/icons/AddCircleOutline'
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline'
import CartEmpty from './CartEmpty'
import { toastInfo } from '../utils/index';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import { removeFromCart } from './Redux/actions/cartActions';
import { connect } from 'react-redux';

const customStyles={
    table:{
        width: '100%',
        maxWidth: '100%',
        marginBottom: 20,
    },
    subimgdiv:{
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 5,
        alignItem: 'center',
        display: 'block',
        width: 80,
        height: 80
    },
    productdetaildiv:{
        width:'100%'
    },
    tableheading:{
        textAlign:'center'
    },
    qtydiv:{
        width:40,
        height:40,
        textAlign:'center'
    },
    updateqtydiv:{
        width:'100%',
        textAlign:'center'
        // display: 'flex',
        // justifyContent: 'center',
    },
    detailsdiv:{
        padding:'3% 0% 3% 3%'
    },
    iconStyle:{
        cursor: 'pointer',
    },
    maindiv:{
        display:'flex'
    },
    subdiv:{
        padding:'3%'
    },
    pricedetailsdiv:{
        display:'flex',
        justifyContent:'space-around'
    },
    textdiv:{
        fontSize:30,
        textAlign:'center',
        paddingBottom:'5%'
    },
    orderbtndiv:{
        textAlign:'center',
        paddingBottom:'6%',
        paddingTop:'5%'
    },
    orderbtn:{
        width:'90%'
    }
}
class ShowCart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userData: props.data,
            userDetails: Decrypt().user,
            cart:[],
            qty:[],
            priceArr:[]
        }
    }
    fetchdata(){
        const {userDetails} = this.state
        var newdata=[]
        axios.get(`${BaseURL}/showcart`, { headers: { Authorization: userDetails.token } })
            .then(response => {
                if(response.data.success)
                {
                    const data = new Array(response.data.cart.length).fill(1)
                    response.data.cart.map((step, index) =>  
                        newdata.push(step.product_id.product_cost) 
                        // = new Array(response.data.cart.length).fill(step.product_id.product_cost)
                    )
                    this.setState({
                        cart:response.data.cart,
                        qty:data,
                        priceArr:newdata
                    },()=>console.log(this.state.priceArr))
                }
            })
            .catch((error) => {
                console.log('error ', error)
            })
    }
    componentDidMount(){
        window.scrollTo(0, 0);
        this.fetchdata()
    }
    deleteProductFromCart(data,index){
        const {userDetails} = this.state
        confirmAlert({
          title: 'Confirm to submit',
          message: 'Are you sure want to remove this item from cart.',
          buttons: [
            {
              label: 'Yes',
              onClick: () => {   
                axios.delete(`${BaseURL}/deletecart/${data._id}`, { headers: { Authorization: userDetails.token } })
                .then(response => {
                    if(response.data.success)
                    {
                        this.props.removeFromCart();
                        this.fetchdata()
                        toastInfo("Product Removed from cart")
                    }
                })
                .catch((error) => {
                    console.log('error ', error)
                })
              }
            },
            {
              label: 'No',
            }
          ]
        });
    }
    handleUpdateQty(index, cost){
        const priceArr = [...this.state.priceArr];
        const qtyArr = [...this.state.qty];
        qtyArr[index] +=  1;
        const updatedPrice = priceArr.map((result, index1) => {
            if(index === index1){
                const updatedVal = cost * qtyArr[index1];
                return updatedVal;
            }
            return result;
        })
        this.setState({
            qty: qtyArr,
            priceArr: updatedPrice
        })
    }
    handleRemoveQty(index,cost){
        const priceArr = [...this.state.priceArr];
        const qtyArr = [...this.state.qty];
        qtyArr[index] -= 1;
        const updatedPrice = priceArr.map((result, index1) => {
            if(index === index1){
                const priceVal = qtyArr[index1] * cost;
                return priceVal
            }
            return result
        })
        this.setState({
            qty: qtyArr,
            priceArr: updatedPrice
        })
    }
    placeOrder(){
        const {cart,qty} = this.state
        let data=[]
        console.log(cart[0].product_id._id,'@@@@')
        // for(var i=0;i<cart.length;i++){
        //     [
        //         data.push(
        //             {
        //                 product_id:cart[i].product_id._id,
        //                 qty:qty[i]

        //             }
        //         )
        //     ]
        // }
        cart.forEach((item,index) => {
            data.push(
                {
                    product_id:item.product_id._id,
                    qty:qty[index]

                }
            )
        })
        window.localStorage.setItem('location','checkout')
        this.props.history.push({
            pathname: '/address',
            state: { data }
        },()=> console.log('===',data))
        return true
    }
    showCartProducts(){
        const {cart,qty,priceArr} = this.state
        return( 
            <Grid style={customStyles.maindiv} container> 
                <Grid style={customStyles.detailsdiv} item xs={8} sm={8} md={8}>
                <Paper>
                <Table aria-label="simple table">
                        <TableHead>
                        <TableRow>
                            <TableCell style={customStyles.tableheading}>Product</TableCell>
                            <TableCell style={customStyles.tableheading}>Quantity</TableCell>
                            <TableCell align="right">Price</TableCell>
                            <TableCell align="right">Total</TableCell>
                            <TableCell align="right">Action</TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                        {cart.map((cart,index) => (
                            <TableRow key={cart.product_id.product_id}>
                                <TableCell style={customStyles.productdetaildiv} component="th" scope="row">
                                    <Grid container spacing={1}>
                                        <Grid item xs={4} >
                                            <img 
                                                style={customStyles.subimgdiv} 
                                                src={`${BaseURL}/${cart.product_id.product_image[0]}`} 
                                                alt={cart.product_id.product_name}
                                            />
                                        </Grid>
                                        <Grid item xs={8} >
                                            <Grid item xs={12} >
                                                {cart.product_id.product_name}
                                            </Grid>
                                            <Grid item xs={12} >
                                                {cart.product_id.product_producer}
                                            </Grid>
                                            <Grid item xs={12} >
                                                <b>Status:</b>
                                                {
                                                    cart.product_id.product_stock===0 ?
                                                    'Out of Stock' :
                                                    'In Stock'
                                                }
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </TableCell>
                                <TableCell style={customStyles.updateqtydiv}>
                                        <Button
                                            style={{minWidth:0,padding:0}}
                                            onClick={()=>this.handleUpdateQty(index, cart.product_id.product_cost)}
                                            disabled={qty[index]===10}

                                        >
                                            <AddCircleOutline 
                                                style={customStyles.iconStyle}
                                            />
                                        </Button>
                                        <input 
                                            style={customStyles.qtydiv} 
                                            value={qty.length===0 ? 1 : qty[index]}
                                            disabled
                                        />
                                        <Button 
                                            style={{minWidth:0,padding:0}}
                                            disabled={qty[index]===1?true:false}
                                            onClick={()=>this.handleRemoveQty(index,cart.product_id.product_cost)}
                                        >
                                            <RemoveCircleOutline 
                                                style={customStyles.iconStyle}
                                            />
                                        </Button>
                                </TableCell>
                                <TableCell align="right">{cart.product_id.product_cost}</TableCell>
                                <TableCell align="right">{qty[index]*cart.product_id.product_cost}</TableCell>
                                <TableCell align="right">
                                    <Button 
                                        style={{minWidth:0,padding:0}}
                                        onClick={()=>this.deleteProductFromCart(cart,index)}
                                    >
                                        <DeleteIcon/>
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                        </TableBody>
            </Table>
            </Paper>
            </Grid>
                <Grid style={customStyles.subdiv} item xs={4} sm={4} md={4}>
                    <Paper style={{padding:'2% 2% 2% 2%',width:'100%'}}>
                        <div style={customStyles.textdiv}>Review Order</div>
                        <Grid style={customStyles.pricedetailsdiv} item xs={12}>
                            <Grid item xs={6}>
                               <b>Subtotal</b> 
                            </Grid>
                            <Grid style={customStyles.tableheading} item xs={6}>
                                {
                                    priceArr.reduce((a, b) => a + b, 0)
                                }
                            </Grid>
                        </Grid>
                        <hr/>
                        <Grid style={customStyles.pricedetailsdiv} item xs={12}>
                            <Grid item xs={6}>
                               <b>Gst(5%)</b> 
                            </Grid>
                            <Grid style={customStyles.tableheading} item xs={6}>
                                {
                                    priceArr.reduce((a, b) => (a + b)*0.05, 0).toFixed(2)
                                }
                            </Grid>
                            
                        </Grid>
                        <hr/>
                        <Grid style={customStyles.pricedetailsdiv} item xs={12}>
                            <Grid item xs={6}>
                               <b>Order Total</b> 
                            </Grid>
                            <Grid style={customStyles.tableheading} item xs={6}>
                                {
                                    parseInt(priceArr.reduce((a, b) => a + b, 0),10)
                                    +
                                    parseInt(priceArr.reduce((a, b) => (a + b)*0.05, 0),10)
                                }
                            </Grid>
                        </Grid>
                        <div style={customStyles.orderbtndiv}>
                            <Button 
                                variant="contained" 
                                color="primary"
                                style={customStyles.orderbtn}
                                onClick={()=>this.placeOrder()}
                            >
                                Proceed To Buy
                            </Button>
                        </div>
                    </Paper>
                </Grid>
            
            </Grid>
        )
    }
    showEmptyCart(){
        return(
            <CartEmpty {...this.props}/>
        )
    }
    render() {
        const {cart} = this.state
        return(
            <div>
                    {cart.length===0 ? this.showEmptyCart() : this.showCartProducts()}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    cartQty: state.pReducer.cartQty
});

const mapDispatchToProps = dispatch => ({
    removeFromCart: () => dispatch(removeFromCart())
});
export default connect(mapStateToProps, mapDispatchToProps)(ShowCart)

