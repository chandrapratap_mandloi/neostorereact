import React, { Component } from 'react'
import Header from './Header'
import Footer from './Footer'
export class Layout extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
    
    render() {
        console.log(this.props.children.props,'LAyout')
        return (
            <div>
               <Header {...this.props}/>
                {this.props.children}
                <Footer /> 
            </div>
        )
    }
}

export default Layout
