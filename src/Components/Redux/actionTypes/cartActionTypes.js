export const ADD_TO_CART = 'cart/addToCart';
export const REMOVE_FROM_CART = 'cart/removeFromCart';
export const Empty_Cart = 'cart/deleteCart'
export const Cart_Count = 'cart/cartcount'
export const isAuthenticated = 'user/loggedin'
export const ON_NETWORK_SUCCESS = 'api/colors'
export const ON_Login_Success = 'api/login'
// export const Checkout = 'product/placed'