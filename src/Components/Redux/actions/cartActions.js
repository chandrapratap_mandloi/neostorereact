import { ADD_TO_CART } from "../actionTypes/cartActionTypes";
import { REMOVE_FROM_CART } from "../actionTypes/cartActionTypes";
import { Empty_Cart } from "../actionTypes/cartActionTypes";
import { Cart_Count } from "../actionTypes/cartActionTypes";
import { isAuthenticated } from "../actionTypes/cartActionTypes";
import { ON_NETWORK_SUCCESS } from "../actionTypes/cartActionTypes";
import {ON_Login_Success} from "../actionTypes/cartActionTypes"
import axios from 'axios'
// import CryptoJS from 'crypto-js'
import BaseURL from '../../Baseurl'
import { toastError,toastSuccess } from '../../../utils';
export const addToCart = () => ({
  type: ADD_TO_CART
});

export const removeFromCart = () => ({
  type: REMOVE_FROM_CART
});

export const deleteCart = () => ({
  type: Empty_Cart
});

export const cartcount = count => ({
  type: Cart_Count,
  payload: count
});

export const isauthenticated = token => ({
  type: isAuthenticated,
  payload: token
});

const onNetworkSuccess = data => ({
  type: ON_NETWORK_SUCCESS,
  data
});

const OnLoginSuccess = data => ({
    type: ON_Login_Success,
    data
})
export const fetchData = () => dispatch =>{
    axios.get(`${BaseURL}/colors`).then(response => {
        dispatch(onNetworkSuccess(response.data));
    }).catch(err => console.log(err));
}


export const userLogin = (values,successCb) => dispatch =>{
  console.log('values from action',values)
  axios.post(`${BaseURL}/login`, values)
    .then(response => {
      if(response.data.success)
      {
        dispatch(OnLoginSuccess(response.data))
        // window.localStorage.setItem('cartcount',CryptoJS.AES.encrypt(JSON.stringify(response.data.cart_count),'secret key 123'))
        //               // window.localStorage.setItem('cartdata',CryptoJS.AES.encrypt(JSON.stringify(cartdata),'secret key 123'))
        //               window.localStorage.setItem('user',CryptoJS.AES.encrypt(JSON.stringify(response.data),'secret key 123'))
        //               window.localStorage.setItem('address',CryptoJS.AES.encrypt(JSON.stringify(response.data.address),'secret key 123'))
        successCb(true)
        toastSuccess('API Success')     
      }
    })
    .catch(error => {
        toastError(error && error.response && error.response.data.message)
        console.log(error.response, 'error')
    })
}