import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import cartReducer from './reducer/cartReducer';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';


const persistConfig = {
    key: 'root',
    storage: storage,
    stateReconciler: autoMergeLevel2 // see "Merge Process" section for details.
   };
   
   const pReducer = persistReducer(persistConfig, cartReducer);
   
   export const store = createStore(
        combineReducers({   
            pReducer
        }), 
        applyMiddleware(thunk)
    );
   export const persistor = persistStore(store);
// const persistConfig = {
//     key: 'root',
//     storage:storage,
//     whitelist:['cartReducer']
//   }
//   console.log(storage.getItem,'####')
//   const persistedReducer = persistReducer(persistConfig, cartReducer)

// export default () => {
//   let store = createStore(combineReducers({
//         persistedReducer
//     }))
//   let persistor = persistStore(store)
//   return { store, persistor }
// }
// const store = createStore(
    // combineReducers({
    //     cartReducer
    // })
// );

// export default store;