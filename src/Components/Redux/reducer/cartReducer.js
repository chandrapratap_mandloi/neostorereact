import { ADD_TO_CART } from '../actionTypes/cartActionTypes';
import {REMOVE_FROM_CART} from '../actionTypes/cartActionTypes'
import {Empty_Cart} from '../actionTypes/cartActionTypes'
import {Cart_Count} from '../actionTypes/cartActionTypes'
import {isAuthenticated} from '../actionTypes/cartActionTypes'
import {ON_NETWORK_SUCCESS} from '../actionTypes/cartActionTypes'
import {ON_Login_Success} from '../actionTypes/cartActionTypes'
import CryptoJS from 'crypto-js'
        var checkdata = window.localStorage.getItem('cartcount')
        if(checkdata){
        var bytes=CryptoJS.AES.decrypt(window.localStorage.getItem('cartcount'), 'secret key 123');
        var cartcount=JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        console.log(bytes,cartcount,'====#####=====')}


const initialState = { cartQty: cartcount, token:null, apidata:null, usertoken:null }
export default function cartReducer(state=initialState, action){ 
    switch(action.type){
        case ADD_TO_CART: 
            return {...state, cartQty: state.cartQty + 1}
        case REMOVE_FROM_CART: 
            return {...state, cartQty: state.cartQty - 1}
            case Empty_Cart:
                return {...state, cartQty : 0}
        case Cart_Count: 
                console.log(action,'reducer')
                return {...state, cartQty: action.payload}
        case isAuthenticated:
                console.log(action,'token')
                return {...state, token: action.payload}
        case ON_NETWORK_SUCCESS:
                return {...state, apidata: action.data }
        case ON_Login_Success: 
                return {...state, usertoken:ON_Login_Success}
        default:
            return state
    }
}