import React, { Component } from 'react'
import {createStore} from 'redux'

 class CartDetails extends Component {
    render() {

        // reducer: store n action
        const cart = function(state, action){
            if(action.type === "addToCart"){
                return action.payload
            }
            if(action.type === "removeFromCart"){
                return action.payload
            }
            if(action.type === "cartQty"){
                return action.payload
            }
            return state
        }

        // Create Store
        const store = createStore(cart,0);
        
        // Subcribe
        store.subscribe(()=>{
            console.log('Store is now', store.getState())
        })
        // Dispatch action
        store.dispatch({type:"addToCart",payload:cart+1})
        store.dispatch({type:"removeFromCart",payload:cart-1})
        store.dispatch({type:"cartQty",payload:"CP"})
        return (
            <div>
                Redux
            </div>
        )
    }
}

export default CartDetails
