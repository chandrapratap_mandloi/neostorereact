import React, { Component } from 'react'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import SwipeableTextMobileStepper from './Carousal'
import '../CSS/dashboard.css'
import axios from 'axios';
import BaseURL from './Baseurl'
import DashProducts from './DashProducts'
import Button from '@material-ui/core/Button';

class Dashboard extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            category:[],
           update: 'blank'
        }
    }
    componentDidMount(){
        // window.scrollTo(0, 0);
        axios.get(`${BaseURL}/allcategory`)
            .then(response => {
                console.log()
                this.setState({
                    category:response.data.product
                })

            })
            .catch(error => { console.log(error, 'error') })
        console.log('component did moun')
    }
    
    UNSAFE_componentWillMount(){
        console.log('will mount')
        // var myPromise = new Promise(function(resolve, reject){
        //     setTimeout(() => {
        //         resolve('foo')
        //     }, 1000);
        // });
        // myPromise.then(res => {
        //     this.setState({
        //         update: res
        //     })
        // })
        new Promise(function(resolve, reject) {

            setTimeout(() => resolve(1), 10000); 
          
          }).then(function(result) { 
          
            //alert(result); 
            return result * 3;
          
          }).then(function(result) { 
          
            // alert(result); 
            return result * 4;
          
          })

          var promise = new Promise(function(resolve, reject) { 
            const x = "geeksforgeeks"; 
            const y = "geeksforgeeks"
            if(x === y) { 
              resolve(); 
            } else { 
              reject(); 
            } 
          }); 
            
          promise
              .then(function () { 
                  console.log('Success, You are a GEEK'); 
              })
              .catch(function () { 
                  console.log('Some error has occured'); 
              }); 
    }

    render() {
       const {category} = this.state
       console.log(this.state.update)
        return (
            // <img src={require('../images/img1.jpg')}  alt="Third slide"/>
            <div>
                
            <div className="container" style={{marginTop:'2%'}}>
              { category.length>0 && <SwipeableTextMobileStepper categories={category} {...this.props}/>}
            </div>
            <div className="container" style={{marginTop:'2%'}}>
              <h2>
                  Popular Products
              </h2>
                <Button onClick={()=>this.props.history.push('/productpage')} color="primary" >
                    View All
                </Button>
            </div>
            <div className="container" style={{marginTop:'2%'}}>
              <DashProducts {...this.props}/>
            </div>
            </div>
        )
    }
}

export default Dashboard
