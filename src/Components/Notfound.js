import React, { Component } from 'react'
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
export class Notfound extends Component {
    render() {
        return (
            <div style={{display:'flex', justifyContent:'center', paddingTop:'2%', paddingBottom:'2%'}}>
                <Paper>
                <Grid style={{display:'flex', justifyContent:'center'}} container>
                    <img src={require('../images/notfound.jpg')} style={{height:400, width:550}} alt="sign up" />
                    <Grid item xs={12}>
                        <Button 
                            onClick={()=>this.props.history.push('/dashboard')}
                            style={{ backgroundColor: '#3f51b5', color: 'white', width:'100%' }}
                        >
                            Homepage
                        </Button>
                    </Grid>
                </Grid>
                </Paper>
            </div>
        )
    }
}

export default Notfound