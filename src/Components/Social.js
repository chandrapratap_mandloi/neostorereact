import React, { Component } from 'react'
import SocialLogin from './SocialLogin'
class Social extends Component {
    
    handleSocialLogin = (user) => {
        console.log('user',user)
      }
       
    handleSocialLoginFailure = (err) => {
        console.error('error',err)
      }
    render() {
        
          
        return (
            <div>
            <SocialLogin
              provider='facebook'
              appId='382148426057043'
              onLoginSuccess={this.handleSocialLogin}
              onLoginFailure={this.handleSocialLoginFailure}
            >
              Login with Facebook
            </SocialLogin>
          </div>
        )
    }
}

export default Social
