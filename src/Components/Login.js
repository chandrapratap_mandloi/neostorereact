import React, { Component } from 'react'
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import BaseURL from './Baseurl';
import axios from 'axios'
// import GoogleLogin from 'react-google-login';
import SocialLogin from './SocialLogin'
// import CryptoJS from 'crypto-js'
import {  GoogleLoginButton, TwitterLoginButton } from "react-social-login-buttons";
import {
  FacebookIcon,
  // TwitterIcon,
} from 'react-share';
import { toastError } from '../utils';
// import { FacebookLoginButton } from "react-social-login-buttons";
import { connect } from 'react-redux';
import { userLogin} from './Redux/actions/cartActions'

const customStyles={
  maindiv:{height:'100%'},
  firstgrid:{
    padding: '4%',
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  textfield:{
    width:270,
    marginBottom:'2%',
    height:40,
    borderRadius:'4%',
    paddingLeft:'2%'
  },
  paper:{
    width:'85%',
    paddingTop:'2%',
    paddingBottom:'2%'
  },
  secondgrid:{
    padding: '2% 2% 2% 2%'
  },
  errordiv:{
    fontSize:12,
    color:'red'
  },
  socialbtn:{
    display: 'flex',
    alignItems: 'center',
    backgroundColor:'#3b5998',
    color:'white',
    border: 0,
    borderRadius: 3,
    cursor: 'pointer',
    fontSize: 19,
    margin: 5,
    width: 'calc(100% - 100px)',
    overflow: 'hidden',
    padding: '0px 10px',
    userSelect: 'none',
    height: 50,
  },
  submitbtn:{
    width: '54%',
    height: 30,
    backgroundColor:'#3f51b5',
    color:'white',
    border:'none'
}
}
const SignupSchema = Yup.object().shape({
  email: Yup.string()
    .email('Invalid email')
    .required('Required'),
    password: Yup.string()
    .min(4, 'Password Must be four characters long!')
    .max(20, 'Too Long!')
    .required('Required'),
});
// regex(/^(?=.*[a-zA-Z])(?=.*[0-9])/)
// const cartdata = [];
class Login extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      logged: false,
      user: {},
      currentProvider: ''
    }
    this.nodes = {}
    this.onLoginSuccess = this.onLoginSuccess.bind(this)
    this.onLoginFailure = this.onLoginFailure.bind(this)
    this.onLogoutSuccess = this.onLogoutSuccess.bind(this)
    this.onLogoutFailure = this.onLogoutFailure.bind(this)
    this.logout = this.logout.bind(this)
  }

  setNodeRef (provider, node) {
    if (node) {
      this.nodes[ provider ] = node
    }
  }
  
  handleSocialLogin = (user) => {
    console.log('user',user)
  }
   
  handleSocialLoginFailure = (err) => {
    console.error('error',err)
  }
  onLoginSuccess (user) {
    console.log(user)
    console.log('login success')
    const data = {accessToken : user._token.accessToken}
    axios.post(`${BaseURL}/social`, data)
                .then(response => {
                    console.log(response)
                })
                .catch(error => {
                    console.log(error, 'error')
                })
    this.setState({
      logged: true,
      currentProvider: user._provider,
      user
    })
  }

  onLoginFailure (err) {
    console.error(err)

    this.setState({
      logged: false,
      currentProvider: '',
      user: {}
    })
  }
  onLogoutSuccess () {
    
    this.setState({
      logged: false,
      currentProvider: '',
      user: {}
    })
  }

  onLogoutFailure (err) {
    console.error(err)
  }

  logout () {
    const { logged, currentProvider } = this.state

    if (logged && currentProvider) {
      this.nodes[currentProvider].props.triggerLogout()
    }
  }
  successCb=(val)=>{
    if(val){
      this.props.history.push('/dashboard')
    }
  }
  render() {
    return (
      <div style={customStyles.maindiv}>
         <Grid container>
          <Grid item xs={6} style={customStyles.firstgrid} column align="center">
            <Grid item xs={12} align="center">
                <SocialLogin
                  provider='facebook'
                  appId='382148426057043'
                  onLoginSuccess={this.onLoginSuccess}
                  onLoginFailure={this.onLoginFailure}
                  style={customStyles.socialbtn}
                  // onLoginSuccess={this.handleSocialLogin}
                  // onLoginFailure={this.handleSocialLoginFailure}
                >
                  <FacebookIcon size={32}/>
                  Login with Facebook
              </SocialLogin>
             
              {/* <GoogleLogin
                style={{width:'100%'}}
                key="IzaSyArXMigrURQA-PvDhmf1nZvVgoW8r6TgjM"
                clientId="AIzaSyArXMigrURQA-PvDhmf1nZvVgoW8r6TgjM"
                
                onSuccess={this.onLoginSuccess}
                onFailure={this.onLoginFailure}
                cookiePolicy={'single_host_origin'}
                >
                  Login with Google
                </GoogleLogin>   */}
              {/* TwitterIcon */}
            
              

            </Grid>
            <Grid item xs={12} align="center">
            <GoogleLoginButton style={{width: 'calc(100% - 100px)'}} onClick={() => alert("Hello")} />
            </Grid>
            <Grid item xs={12} align="center">
            <TwitterLoginButton style={{width: 'calc(100% - 100px)'}} onClick={() => alert("Hello")} />

            </Grid>
            {/* google */}
          </Grid>
          <Grid item xs={6} style={customStyles.secondgrid} column align="center">
            <Paper style={customStyles.paper}>
            <h1>Login to NeoSTORE</h1>
            <Formik
              initialValues={{
                email: '',
                password:''
              }}
              validationSchema={SignupSchema}
              onSubmit={values => {
                toastError('Inside')
                console.log(values,'=======')
                this.props.userLogin(values,this.successCb)
                // axios.post(`${BaseURL}/login`, values)
                // .then(response => {
                //     if(response.data.success)
                //     {
                //       console.log(response.data.cart_count)
                //       response.data.cart.map((step, index) => 
                //           cartdata.push(step.product_id._id)
                //           // console.log(step.product_id._id,'Step*******')
                //       )
                //       this.props.dispatch(cartcount(response.data.cart_count))
                //       this.props.dispatch(isauthenticated(response.data.token))
                //       window.localStorage.setItem('cartcount',CryptoJS.AES.encrypt(JSON.stringify(response.data.cart_count),'secret key 123'))
                //       window.localStorage.setItem('cartdata',CryptoJS.AES.encrypt(JSON.stringify(cartdata),'secret key 123'))
                //       window.localStorage.setItem('user',CryptoJS.AES.encrypt(JSON.stringify(response.data),'secret key 123'))
                //       window.localStorage.setItem('address',CryptoJS.AES.encrypt(JSON.stringify(response.data.address),'secret key 123'))
                //       this.props.history.push('/dashboard')
                //     }
                // })
                // .catch(error => {
                //   toastError(error && error.response && error.response.data.message)
                //     console.log(error.response, 'error')
                //     // console.log(JSON.stringify(error), 'error')
                // })
                // same shape as initial values
                console.log(values, this.props);
              }}
            >
              {({ errors, touched }) => (
                <Form>
                  <Grid item xs={12} align="center">
                  <Field style={customStyles.textfield} placeholder="Email" name="email" type="email" />
                  {errors.email && touched.email ? <div style={customStyles.errordiv}>{errors.email}</div> : null}
                  </Grid>
                  <Grid item xs={12} align="center">
                  <Field style={customStyles.textfield} placeholder="Enter Password" name="password" type="password" />
                  {errors.password && touched.password ? <div style={customStyles.errordiv}>{errors.password}</div> : null}
                  </Grid>
                  <button style={customStyles.submitbtn} type="submit">Submit</button>
                </Form>
              )}
            </Formik>
            </Paper>
            </Grid>
        </Grid>
      </div>
    )
  }
}

// export default Login
const mapStateToProps = state => (
  {
  cartQty: state.pReducer.cartQty,
  isAuthenticated:state.pReducer.isAuthenticated
});
const mapDispatchToProps = dispatch => ({
  userLogin: (values, successCb) => dispatch(userLogin(values,successCb))
});
export default connect(mapStateToProps,mapDispatchToProps)(Login)