import React, { Component } from 'react'
import Decrypt from './Userdata'
import axios from 'axios'
// import BasURL from './Baseurl'
import Grid from '@material-ui/core/Grid';
import BaseURL from './Baseurl'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import moment from 'moment'
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import '../CSS/user.css'

// import ButtonGroup from '@material-ui/core/ButtonGroup';

const customStyles={
    table:{
        width: '100%',
        maxWidth: '100%',
        marginBottom: 20,
    },
    maindiv:{
        paddingBottom:'4%'
    },
    imagestyle:{
        borderRadius: '50%',
        height: 130,
    },
    headingdiv:{
        fontSize:30,
        color:'red',
        paddingTop:'2%',
        paddingLeft:'5%'
    },
    namediv:{
        fontSize:20,
        color:'#3f51b5'
    },
    btn:{
        color:'#3f51b5'
    },
    userdetails:{
        width:'80%',
        height:'100%'
    },
    editbtngrid:{
        marginBottom:'2%',
        marginTop:'2%',
        display: 'flex',
        justifyContent: 'space-around'
    },
    btngrid:{
        marginBottom:'2%',
        marginTop:'2%'
    },
    radiodiv:{
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    profilediv:{
        fontSize: 25,
        paddingTop: '1%',
        paddingLeft: '1%',
        paddingRight: '1%'
    },
    themebtn:{ backgroundColor: '#3f51b5', color: 'white' }
}
class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userData: props.data,
            userDetails: Decrypt().user,
            toggle:false,
            first_name:'',
            lastname:'',
            email:'',
            phone:'',
            dob:'',
            gender:'',
            profile_img:'',
            usrimg:''
        }
        this.attachMenu = this.attachMenu.bind(this);
    }
    attachMenu(event){
        console.log('inside')
        let reader = new FileReader();
        let output = document.getElementById('file');
        const array = []

    reader.onload = function(){
      output.src = reader.result;
      array.push(output.src)
    }
    setTimeout(() => 
        this.setState({
        usrimg:output.src
    })
        , 1000);
    // this.setState({
    //     usrimg:base64
    // },()=> console.log(this.state.usrimg))
    if(event.target.files[0]){
      reader.readAsDataURL(event.target.files[0]);
    }
    }
    
    handlefileupload = (event) => {
        this.setState({
            profile_img:event.target.files[0]
        },()=> console.log(this.state.profile_img))
        
    }
    handleUpdateProfile= (event) =>
    {
        console.log('inside update')
        const {userDetails,userData,usrimg} = this.state
        console.log('dfdfsdf************',userData)
        event.preventDefault()
        const formData = new FormData();
        formData.append('first_name', userData.first_name);
        formData.append('last_name', userData.last_name);
        formData.append('email', userData.email);
        formData.append('phone', userData.phone);
        // formData.append('gender', userData.gender);
        formData.append('gender', 1);
        formData.append('dob', userData.dob);
        formData.append('profile_img',this.state.profile_img);
        // formData.append('profile_img',usrimg);

        const data={
            first_name:userData.first_name,
            last_name:userData.last_name,
            email:userData.email,
            phone:userData.phone,
            gender:userData.gender,
            dob:userData.dob,
            profile_img:usrimg
        }
        console.log(data,'data===>')
        const config = {
            headers: {
                // 'content-type': 'multipart/form-data',
                'Authorization':`${userDetails.token}`
            }
        };
        // updateprofile editprofile
        axios.post(`${BaseURL}/editprofile`,formData,config)
            .then((response) => {
                console.log(response)
                if(response.data.success)
                {
                    this.setState({
                        toggle:false
                    })
                    // this.props.cbFn(userData.first_name,response.data.customer.profile_img)
                    this.props.cbFn(userData.first_name,response.data.customer.user_img)
                }
            }).catch((error) => {
        });
    }
    showUserProfile()
    {
        const {userData} = this.state
        return(
            <div>
                    <Grid style={customStyles.profilediv} item xs={12} >
                        <b>Profile</b>
                        <hr/>
                    </Grid>
                     <table style={{width:'100%'}}>
                     <tbody>
                                    <tr>
                                        <th>First Name:</th>
                                        <td>{userData.first_name}</td>
                                    </tr>
                                    <tr>
                                        <th>Last Name:</th>
                                        <td>{userData.last_name}</td>
                                    </tr>
                                    <tr>
                                        <th>Email:</th>
                                        <td>{userData.email}</td>
                                    </tr>
                                    <tr>
                                        <th>Phone:</th>
                                        <td>{userData.phone}</td>
                                    </tr>
                                    <tr>
                                        <th>Date of Birth:</th>
                                        <td>{userData.dob? moment(userData.dob).format('Do MMM YY') : 'DD/MM/YYYY'}</td>
                                    </tr>
                                    <tr>
                                        <th>Gender:</th>
                                        <td>{userData.gender===1 ? 'Male' : 'Female'}</td>
                                    </tr>
                                    </tbody>
                            </table>
                            
                            <Grid style={customStyles.btngrid} item xs={12}>
                            <hr/>
                            <Button onClick={()=> this.setState({toggle:true})} style={{ backgroundColor: '#3f51b5', color: 'white' }}>
                                Edit Profile
                            </Button>
                            </Grid>
            </div>
        )
    }
    handlechange=(event,type)=>{
        const {userData} = this.state
        var obj={...userData}
        if(type==='first_name')
        {
            obj.first_name=event.target.value
        }
        else if(type==='last_name')
        {
            obj.last_name=event.target.value
        }
        else if(type==='email')
        {
            obj.email=event.target.value
        }
        else if(type==='phone')
        {
            obj.phone=event.target.value
        }
        else if(type==='gender')
        {
            obj.gender=event.target.value
        }
        else if(type==='dob')
        {
            obj.dob=event.target.value
        }
        this.setState({ userData: obj });
    }
    showEditProfile(){
        const {userData} = this.state
        return(
            <div>
                <Grid style={customStyles.profilediv} item xs={12} >
                        <b>Edit Profile</b>
                        <hr/>
                </Grid>
            
            <form onSubmit={this.handleUpdateProfile}>
                <table style={{width:'100%'}}>
                    <tbody>
                        <tr>
                            <th>First Name:</th>
                            <td>
                                <TextField
                                    required
                                    id="standard-required"
                                    // label="First Name"
                                    // onChange={this.handleNameChange}
                                    onChange={()=>this.handlechange(event,'first_name')}
                                    defaultValue={userData.first_name}
                                    margin="normal"
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>Last Name:</th>
                            <td>
                                <TextField
                                    required
                                    id="standard-required"
                                    // label="First Name"
                                    onChange={()=>this.handlechange(event,'last_name')}
                                    // onChange={this.handleLastNameChange}
                                    defaultValue={userData.last_name}
                                    margin="normal"
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>Email:</th>
                                <td>
                                    <TextField
                                        required
                                        id="standard-required"
                                        onChange={()=>this.handlechange(event,'email')}
                                        // onChange={this.handleEmailChange}
                                        // label="First Name"
                                        defaultValue={userData.email}
                                        margin="normal"
                                    />
                                </td>
                        </tr>
                                    <tr>
                                        <th>Phone:</th>
                                        <td>
                                        <TextField
                                                required
                                                id="standard-required"
                                                // label="First Name"
                                                onChange={()=>this.handlechange(event,'phone')}
                                                // onChange={this.handlePhoneChange}
                                                defaultValue={userData.phone}
                                                margin="normal"
                                            />
                                            </td>
                                    </tr>
                                    <tr>
                                        <th>Date of Birth:</th>
                                        <td>
                                            <TextField
                                                required
                                                id="date"
                                                // label="Start Date"
                                                // variant="outlined"
                                                onChange={()=>this.handlechange(event,'dob')}
                                                // onChange={this.handledobChange}
                                                type="date"
                                                margin="normal"
                                                value={userData.dob}
                                                // maxDate={maxDate1}
                                                // onChange={this.handleStartdate}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Gender:</th>
                                        <td>
                                            <RadioGroup 
                                                aria-label="gender" 
                                                value={userData.gender===1 ? '1' : '0'} 
                                                name="gender2" 
                                                onChange={()=>this.handlechange(event,'gender')}
                                                // onChange={this.handleGenderChange} 
                                                style={customStyles.radiodiv}
                                            >
                                                <FormControlLabel
                                                    value="0"
                                                    control={<Radio color="primary" />}
                                                    label="Female"
                                                    labelPlacement="start"
                                                />
                                                <FormControlLabel
                                                    value="1"
                                                    control={<Radio color="primary" />}
                                                    label="Male"
                                                    labelPlacement="start"
                                                />
                                            </RadioGroup> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Profile Picture:</th>
                                        <td>
                                            <TextField
                                                required
                                                id="file"
                                                // label="Start Date"
                                                // variant="outlined"
                                                onChange={this.handlefileupload}
                                                // onChange={this.attachMenu}
                                                type="file"
                                                margin="normal"
                                            />
                                        </td>
                                    </tr>
                                    </tbody>
                            </table>
                            <hr/>
                            <Grid style={customStyles.editbtngrid} item xs={12}>
                            
                                    <Button type="submit"  style={customStyles.themebtn}>
                                        Update Profile
                                    </Button>
                                    <Button type="button"  onClick={()=> this.setState({toggle:false})} style={customStyles.themebtn}>
                                        Cancel
                                    </Button> 
                            </Grid>
                            </form>
                            </div>
        )
    }
    render() {
        const {toggle} = this.state
        return (
            <div>
                <Grid style={customStyles.maindiv} container>
                    <Grid style={{paddingLeft:'8%'}} container >
                        { toggle ? this.showEditProfile() : this.showUserProfile()}
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default Profile
