import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';

const customStyles={
    cardstyle:{
        width:'80%'
    },
    maindiv:{
        display:'flex',
        justifyContent:'center',
        paddingBottom:'2%'
    },
    subdiv:{
        display:'flex',
        justifyContent:'center',
    },
    mainheading:{
        fontSize: 76,
        fontWeight: 900,
        marginTop: 50,
        marginBottom: 50
    }
}
class OrderPlaced extends Component {
    render() {
        return (
            <div style={customStyles.maindiv}>                
                <Card style={customStyles.cardstyle}>
                    <CardContent>
                        <Grid container>
                            <Grid style={customStyles.subdiv} item xs={12}>
                                <h1 style={customStyles.mainheading}>
                                    Thank you for your order
                                </h1>
                            </Grid>
                            <Grid style={customStyles.subdiv} item xs={12}>
                                <h3>
                                    Your order has been placed and is being processed
                                </h3>
                            </Grid>
                        </Grid>
                    </CardContent>
                    <CardActions style={customStyles.subdiv}>
                        <Button 
                            color="primary" 
                            size="small"
                            onClick={()=>this.props.history.push('/dashboard')}
                        >
                            Go To Dashboard
                        </Button>
                    </CardActions>   
                </Card>
            </div>
        )
    }
}

export default OrderPlaced
