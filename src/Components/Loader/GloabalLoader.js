import React from "react";
var Loader = require('react-loader');
import './loader.css'

class SpinnerPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    //   loading: true
    }
  }
  render() {
    return (
    //   <div style={{position:'fixed', top:'40%', left:'50%', zIndex:99}} className='sweet-loading'>
    //     <Loader
    //         type="Bars"
    //         color="#607d8b"
    //         height={100}
    //         width={100}
    //         // visible={true}
    //         visible={this.props.visible}
    //   />
    //   </div> 
        <div>
            <Loader loaded={this.props.visible} lines={13} length={20} width={10} radius={30}
                corners={1} rotate={0} direction={1} color="#000" speed={1}
                trail={60} shadow={true} hwaccel={false} className="spinner"
                zIndex={2e9} top="50%" left="50%" scale={1.00}
                loadedClassName="loadedContent" />
        </div>
    )
  }
}

export default SpinnerPage;