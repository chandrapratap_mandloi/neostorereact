import React from 'react';
import Badge from '@material-ui/core/Badge';
import Box from '@material-ui/core/Box';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
// import Cartdata from './Cartdata'

// const cartdata=Cartdata().cart
// console.log(cartdata.length,']]]]]')
const StyledBadge1 = withStyles(theme => ({
  badge: {
    right: -3,
    border: `2px solid ${theme.palette.background.paper}`,
    padding: '0 4px',
  },
}))(Badge);

export default function CarButton() {
  return (
    <Box display="flex">
      <Box m={1}>
        <IconButton aria-label="cart">
          <StyledBadge1 badgeContent={5} color="primary">
            <ShoppingCartIcon  style={{color:'white'}}/>
          </StyledBadge1>
        </IconButton>
      </Box>
    </Box>
  );
}

// const mapStateToProps = state => ({
//   cartQty: state.cartReducer.cartQty
// });

// const mapDispatchToProps = dispatch => ({
//   addToCart: () => dispatch(addToCart())
// });