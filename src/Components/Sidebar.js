import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import BaseURL from './Baseurl'
import Button from '@material-ui/core/Button';
import AccountBoxIcon from '@material-ui/icons/AccountBox'
import ReorderIcon from '@material-ui/icons/Reorder'
const customStyles={
    table:{
        width: '100%',
        maxWidth: '100%',
        marginBottom: 20,
    },
    maindiv:{
        paddingBottom:'4%'
    },
    imagestyle:{
        borderRadius: '50%',
        height: 130,
    },
    headingdiv:{
        fontSize:30,
        color:'red',
        paddingTop:'2%',
        paddingLeft:'5%'
    },
    namediv:{
        fontSize:20,
        color:'#3f51b5'
    },
    btn:{
        color:'#3f51b5'
    },
    userdetails:{
        width:'80%',
        height:'100%'
    },
    btngrid:{
        marginBottom:'2%',
        marginTop:'2%'
    },
    radiodiv:{
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    profilediv:{
        fontSize: 25,
        paddingTop: '1%',
        paddingLeft: '1%',
        paddingRight: '1%'
    }
}
class Sidebar extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
    ModifyString()
    {
        if(this.props.profile_img)
        {
            var image = this.props.profile_img.split(' ');
            var new_string = image.join('%20');
            return(
            <img style={customStyles.imagestyle} src={`${BaseURL}/${new_string}`} alt="Profile" />
        )
        }
        else{
            return(
                <img style={customStyles.imagestyle} src={require('../images/profile.png')} alt="Profile" />
            )
        }
    }
    render() {
        return (
            <Grid container>
                <Grid item xs={12}  align="center">
                    {this.ModifyString()}
                    {/* <img src={`${BaseURL}/${userData[0].profile_img}`} /> */}
                </Grid>
                <Grid style={customStyles.namediv} item xs={12}  align="center">
                    {this.props.name}
                </Grid>
                <Grid item xs={12}  align="center">
                    <Button style={customStyles.btn}> <AccountBoxIcon/>Profile</Button>
                </Grid>
                <Grid item xs={12}  align="center">
                    <Button style={customStyles.btn}> <ReorderIcon/>Orders</Button>
                </Grid>
                <Grid item xs={12}  align="center">
                    <Button style={customStyles.btn}> <ReorderIcon/>Address</Button>
                </Grid>
            </Grid>
        )
    }
}

export default Sidebar
