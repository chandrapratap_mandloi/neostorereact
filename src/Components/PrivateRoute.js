import React from 'react';

import { Route, Redirect } from 'react-router-dom';

// import { connect } from 'react-redux'

const PrivateRoute = ({ component: Component, token, ...rest }) => {
//   const isLogin = token !== null && token.trim() ? true : false;
  const path =  localStorage.getItem('location') ? localStorage.getItem('location') : null
  return (
    <Route {...rest} render={props => (
      path ?
        <Component {...props} />
        :
        <Redirect to="/login" />
    )} />
  );
};

// const mapStateToProps = (state) => ({
//   token: state.auth.token
// })

export default PrivateRoute
// export default connect(mapStateToProps)(PrivateRoute);