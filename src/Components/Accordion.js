import React, { Component } from 'react'
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemPanel,
    AccordionItemButton,
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';
import ListItem from '@material-ui/core/ListItem';
import axios from 'axios'
import BaseURL from './Baseurl';
import Grid from '@material-ui/core/Grid';
class AccordionSide extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            color:[],
            category:[]
        }
    }
    fetchColors(){
        axios.get(`${BaseURL}/colors`)
            .then(response => {
                this.setState({
                    color:response.data.colors
                })

            })
            .catch(error => { console.log(error, 'error') })
    }
    fetchCategory(){
        axios.get(`${BaseURL}/allcategory`)
            .then(response => {
                this.setState({
                    category:response.data.product
                })

            })
            .catch(error => { console.log(error, 'error') })
    }
    componentDidMount() { 
        window.scrollTo(0, 0);   
        this.fetchCategory()
        this.fetchColors()
            
    }
    sortCategory(step){
        this.props.history.push({
            pathname:`/productpage/${step._id}`,
          }) 
    }
    sortColor(step){
        if(this.props.match.params.id)
        {
            this.props.history.push({
                pathname:`/productpage/${this.props.match.params.id}/${step.color_id}`
            }) 
        }
        else{
            this.props.history.push({
                pathname:`/productpage/${null}/${step.color_id}`
              }) 
            
        }
    }
    render() {
        const {category, color} = this.state
        return category.length>0 && color.length>0 && (
            <Accordion allowZeroExpanded={true} >
            <AccordionItem >
                <AccordionItemHeading>
                    <AccordionItemButton>
                        Category
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                {category.map((step, index) =>  
                <div key={index}>
                    <ListItem button onClick={()=>this.sortCategory(step)}>
                        {step.category_name}
                    </ListItem>  
                    <hr/> 
                    </div>         
                )}
                </AccordionItemPanel>
            </AccordionItem>
            <AccordionItem >
                <AccordionItemHeading>
                    <AccordionItemButton>
                        Color
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                <Grid container  spacing={3}>
                    {color.map((step, index) =>  
                    <Grid key={index} style={{padding:0}} item xs={4}>
                        <ListItem button
                            data-toggle="tooltip"
                            data-placement="left" 
                            title={step.color_name}
                        >
                        <button   onClick={()=>this.sortColor(step)} style={{backgroundColor:`${step.color_code}`,height:30,width:30}}/>
                        </ListItem>
                        </Grid>
                )}</Grid>
                </AccordionItemPanel>           
            </AccordionItem>
        </Accordion>
        )
    }
}

export default AccordionSide
