import React, { Component } from 'react'
import {createStore} from 'redux'
 class ReduxDemo extends Component {
    render() {

        // reducer: store n action
        const reducer = function(state, action){
            if(action.type === "ATTACK"){
                return action.payload
            }
            if(action.type === "NOTATTACK"){
                return action.payload
            }
            if(action.type === "NOTATACK"){
                return action.payload
            }
            return state
        }

        // Create Store
        const store = createStore(reducer,"Peace");
        
        // Subcribe
        store.subscribe(()=>{
            console.log('Store is now', store.getState())
        })
        // Dispatch action
        store.dispatch({type:"ATTACK",payload:"Parth"})
        store.dispatch({type:"NOTATTACK",payload:"MAndloi"})
        store.dispatch({type:"NOTATACK",payload:"CP"})
        return (
            <div>
                Redux
            </div>
        )
    }
}

export default ReduxDemo
