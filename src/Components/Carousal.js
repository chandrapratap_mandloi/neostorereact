import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import BaseURL from './Baseurl';
// import '../CSS/dashboard.css'
const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: 10000,
    flexGrow: 1,
    // border:'2px solid'
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    height: 50,
    paddingLeft: theme.spacing(4),
    backgroundColor: theme.palette.background.default,
  },
  container:{
    position: 'relative',
    textAlign: 'center',
    color: 'black',
    display:'flex',
    justifyContent:'center'
  },
  text:{
    position: 'absolute',
    bottom: 8,
    left: '50%',
    backgroundColor:'white'
  },
  img: {
    height: 400,
    display: 'block',
    maxWidth: 10000,
    overflow: 'hidden',
    width: '80%',
    cursor: 'pointer'
  },
  mainclass:{
      display:'flex',
      justifyContent:'center'
  }
}));

function SwipeableTextMobileStepper(props) {
    const {categories} = props
    const classes = useStyles();
    const theme = useTheme();
    const [activeStep, setActiveStep] = React.useState(0);
    const maxSteps = categories.length;

    function handleNext() {
      setActiveStep(prevActiveStep => prevActiveStep + 1);
      console.log(maxSteps, activeStep)
    }
    function imageClicked(step){
      console.log('image Clicked',props)
        props.history.push({
        pathname:`productpage/${step._id}`,
        state: {step}
      }) 
    }
    function handleBack() {
      setActiveStep(prevActiveStep => prevActiveStep - 1);
    }

    function handleStepChange(step) {
      setActiveStep(step);
    }

  return (
      <div className={classes.mainclass}>
      <div className={classes.root}>
      
      <AutoPlaySwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={activeStep}
        onChangeIndex={handleStepChange}
        enableMouseEvents
      >
        {categories.map((step, index) => (
          <div key={step.category_name}>
            <div className={classes.container}>
                {Math.abs(activeStep - index) <= 2 ? (
                    <img onClick={()=>imageClicked(step)} className={classes.img} src={`${BaseURL}/${step.category_image}`} alt={step.label} />
                ) : null}
                <div className={classes.text}>{categories[activeStep].category_name}</div>
            </div>
          </div>
          
        ))}
      </AutoPlaySwipeableViews>
      <MobileStepper
        steps={maxSteps}
        position="static"
        variant="progress"
        activeStep={activeStep}
        className={classes.container}
        nextButton={
          <Button size="small" onClick={handleNext} disabled={activeStep === maxSteps - 1}>
            Next
            {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
          </Button>
        }
        backButton={
          <Button size="small" onClick={handleBack}  disabled={activeStep === 0}>
            {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            Back
          </Button>
        }
      />
    </div>
    </div>
  );
}

export default SwipeableTextMobileStepper;