import React, { Component } from 'react'
import Decrypt from './Userdata'
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
// import CardActionArea from '@material-ui/core/CardActionArea';
import Button from '@material-ui/core/Button';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Addaddress from './Addaddress'
import { connect } from 'react-redux';
import axios from 'axios'
import BaseURL from './Baseurl'
import CardHeader from '@material-ui/core/CardHeader';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import '../CSS/user.css'
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import EditAddress from './EditAddress'
import DeleteIcon from '@material-ui/icons/Delete';
import AddressLocation from './AddressLocation'
import CryptoJS from 'crypto-js'
import { deleteCart } from './Redux/actions/cartActions';
const customStyles={
    table:{
        width: '100%',
        maxWidth: '100%',
        marginBottom: 20,
    },
    maindiv:{
        paddingLeft: '8%',
        paddingRight: '8%',
        paddingTop: '4%',
    },
    imagestyle:{
        borderRadius: '50%',
        height: 130,
    },
    headingdiv:{
        fontSize:30,
        color:'red',
        paddingTop:'2%',
        paddingLeft:'5%'
    },
    namediv:{
        fontSize:20,
        color:'#3f51b5'
    },
    btn:{
        color:'#3f51b5'
    },
    userdetails:{
        width:'80%',
        height:'100%'
    },
    btngrid:{
        marginBottom:'2%',
        marginTop:'2%'
    },
    radiodiv:{
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    profilediv:{
        fontSize: 25,
        paddingTop: '1%',
        paddingLeft: '1%',
        paddingRight: '1%'
    },
    card:{
        marginBottom:'2%'
    },    
    themebtn:{ marginBottom:'2%',marginLeft:'2%' },
    deleteicon:{color:'red',cursor: 'pointer'},
    cardheader:{display:'flex',height: 0,padding:10}
}
class Address extends Component {
    constructor(props) {
        console.log(props,'props----')
        super(props)
    
        this.state = {
            userDetails: Decrypt().user,
            // userAddress: UserAddress().address,
            userAddress:[],
            toggle:false,
            modal: false,
            deletemodal:false,
            reportmodal:false,
            selectedIndex:undefined,
            location:AddressLocation().checkdata,
            addressid:'',
            placebtn:true,
            product:props.location
            // .state
        }
    }
   
    fetchdata(){
        const {userDetails} = this.state
        axios.get(`${BaseURL}/showaddress`, { headers: { Authorization: userDetails.token } })
            .then(response => {
                if(response.data.success)
                {
                    this.setState({
                        userAddress:response.data.customer
                    })
                }
            })
            .catch((error) => {
                console.log('error ', error)
            })
    }
    componentDidMount(){
        window.scrollTo(0, 0);
        this.fetchdata()
    }
    modalclose = (value, isUpdated) => {
        this.setState({
            modal: value
        })
        if (isUpdated) {
            this.fetchdata()
        }
    }
    addComment (index) {
        this.setState({modal: true,selectedIndex:index})
    }
    
    // deletemodalclose = ()
    submit(data) {
        this.setState({
            deletemodal:true
        })
        const {userDetails} = this.state
        console.log('clicked')
        confirmAlert({
          title: 'Confirm to submit',
          message: 'Are you sure want to delete address.',
          buttons: [
            {
              label: 'Yes',
              onClick: () => {
                axios.delete(`${BaseURL}/deleteaddress/${data._id}`, { headers: { Authorization: userDetails.token } })
                .then(response => {
                    
                    if(response.data.success)
                    {
                        this.fetchdata()
                    }
                })
                .catch((error) => {
                    console.log('error ', error)
                })
              }
            },
            {
              label: 'No',
            //   onClick: () => alert('Click No')
            }
          ]
        });
      };
    deleteaddress(data){
        // console.log('icon clicked',data._id)
        // const {userDetails} = this.state
        
    }

    handlechange=(event)=>{
        
        // obj.gender=event.target.value
        this.setState({ addressid: event.target.value, placebtn:false },()=>console.log(this.state.addressid,'@@33@@'));
    }

    showUserAddress(){
        const {userAddress,selectedIndex, location} = this.state
        return userAddress.length>0 &&(
            <div>
                {userAddress.map((step, index) =>  
                        <Grid key={index}  item xs={12}>
                            <Card style={customStyles.card}
                                // onClick={()=>this.addComment(index)}
                            >
                                 <CardHeader
                                    style={customStyles.cardheader}
                                    action={
                                        <DeleteIcon  
                                            data-toggle="tooltip"
                                            data-placement="left" 
                                            title="Delete Address"
                                            onClick={()=>this.submit(step)}
                                            // onClick={()=>this.deleteaddress(step)}  
                                            style={customStyles.deleteicon} 
                                        />
                                    }
                                />
                                 {selectedIndex===index && <EditAddress address={step}  cbClose={this.modalclose}  isVisible={this.state.modal} />}
                                {/* <CardActionArea> */}
                                    <CardContent >
                                        <Typography gutterBottom  component="h2">
                                            {step.address}
                                        </Typography>
                                        <Typography gutterBottom  component="h2">
                                            {step.city}-{step.pincode}
                                        </Typography>
                                        <Typography gutterBottom  component="h2">
                                            {step.state}
                                        </Typography>
                                        <Typography  gutterBottom  component="h2">
                                            {step.country}
                                        </Typography>
                                        <Typography  style={{display:'flex'}}  component="h2">
                                            {/* <Button onClick={()=>this.addComment(index)} type="submit"  style={customStyles.themebtn}>
                                                Edit 
                                                className="edit_hover_class"
                                            </Button> */}
                                            {location==='checkout' && 
                                                <RadioGroup 
                                                    aria-label="position" 
                                                    name="position"  
                                                    // onChange={this.handleChange} 
                                                    onChange={()=>this.handlechange(event)}
                                                >
                                                    <FormControlLabel
                                                        id={index}
                                                        value={step._id}
                                                        control={<Radio color="primary" />}
                                                        label="Select"
                                                        labelPlacement="end"
                                                    />
                                                </RadioGroup>
                                            }
                                            {/* <Button 
                                                data-toggle="tooltip"
                                                data-placement="left" title="Edit Address"
                                                onClick={()=>this.addComment(index)} 
                                                type="submit"  
                                                style={customStyles.themebtn} 
                                                variant="outlined" color="primary"
                                            >
                                                Edit
                                            </Button> */}
                                        </Typography>
                                    </CardContent>
                                {/* </CardActionArea> */}
                                <Button 
                                                data-toggle="tooltip"
                                                data-placement="left" title="Edit Address"
                                                onClick={()=>this.addComment(index)} 
                                                type="submit"  
                                                style={customStyles.themebtn} 
                                                variant="outlined" color="primary"
                                >
                                                Edit
                                            </Button>
                            </Card>
                        </Grid>
                    )}
                    
            </div>
        )
    }
    setAddress =(val) => {
        this.setState({toggle:val},()=>this.fetchdata())
    }
    placeOrder(){
        const {product,addressid,userDetails} = this.state
        const config = {
            headers: {
                'Authorization':`${userDetails.token}`
            }
        }
        console.log(this.props,'pp009988776')
        const placeorder={
            products: product.state.data,
            address_id:addressid,
            flag:true
        }
            axios.post(`${BaseURL}/cart`,placeorder,config)
            .then(response =>{
                if(response.data.success)
                {
                    this.props.deleteCart()
                    window.localStorage.setItem('cartdata',CryptoJS.AES.encrypt(JSON.stringify([]),'secret key 123'))
                    // window.localStorage.setItem('cartcount',CryptoJS.AES.encrypt(0,'secret key 123'))
                    window.localStorage.removeItem('location')
                    this.props.history.push('/orderplaced')
                }
            })
            .catch(error => {console.log(error,'error')})
    }
    render() {
        const {location,toggle,placebtn} = this.state
        return (
            <div style={customStyles.maindiv}>
                <div>
                    {toggle ? <Addaddress cbFn={this.setAddress}/> : this.showUserAddress()}
                </div> 
                <hr/>
                    <Button onClick={()=>this.setState({toggle:true})} style={customStyles.themebtn} variant="outlined" color="primary">
                        Add Address
                    </Button>  

                    {
                        location==='checkout' && 
                        <Button 
                            onClick={()=>this.placeOrder()} 
                            style={customStyles.themebtn} 
                            variant="outlined" 
                            color="primary"
                            disabled={placebtn}
                        >
                            Place Order
                        </Button>
                    }
            </div>
        )
    }
}


const mapStateToProps = state => ({
    cartQty: state.pReducer.cartQty
});

const mapDispatchToProps = dispatch => ({
    deleteCart: () => dispatch(deleteCart())
});

export default connect(mapStateToProps, mapDispatchToProps)(Address)
// export default Address
