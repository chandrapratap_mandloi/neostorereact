import React, { Component } from 'react'
import Decrypt from './Userdata'
import axios from 'axios'
import Grid from '@material-ui/core/Grid';
import BaseURL from './Baseurl'
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import AccountBoxIcon from '@material-ui/icons/AccountBox'
import ReorderIcon from '@material-ui/icons/Reorder'
import Profile from './Profile'
import Address from './Address'
import Isauth from './Isauth'
import { toastError } from '../utils/index';
import Order from './Order'
import {zoomIn} from 'react-animations'
import Radium, {StyleRoot} from 'radium';

const styles = {
    bounce: {
    animation: 'x 1s',
    animationName: Radium.keyframes(zoomIn, 'zoomIn')
  }
}
const customStyles={
    table:{
        width: '100%',
        maxWidth: '100%',
        marginBottom: 20,
    },
    maindiv:{
        paddingBottom:'4%'
    },
    imagestyle:{
        borderRadius: '50%',
        height: 130,
    },
    headingdiv:{
        fontSize:30,
        color:'red',
        paddingTop:'2%',
        paddingLeft:'5%'
    },
    namediv:{
        fontSize:20,
        color:'#3f51b5'
    },
    btn:{
        color:'#3f51b5'
    },
    userdetails:{
        width:'80%',
        height:'100%'
    },
    editbtngrid:{
        marginBottom:'2%',
        marginTop:'2%',
        display: 'flex',
        justifyContent: 'space-around'
    },
    btngrid:{
        marginBottom:'2%',
        marginTop:'2%'
    },
    radiodiv:{
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    profilediv:{
        fontSize: 25,
        paddingTop: '1%',
        paddingLeft: '1%',
        paddingRight: '1%'
    },
    themebtn:{ backgroundColor: '#3f51b5', color: 'white' }
}
class UserDrawer extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            userDetails: Decrypt().user,
            userData:{},
            drawercontent:'',
            updatedValues:{},
            authenticated:Isauth()
        }
    }
    fetchdata(){
        const {userDetails} = this.state
        axios.get(`${BaseURL}/getuser`, { headers: { Authorization: userDetails.token } })
            .then(response => {
                if(response.data.success)
                {
                    this.setState({
                        userData:response.data.customer[0]
                    })
                }
            })
            .catch((error) => {
                console.log('error ', error)
            })
    }
    componentDidMount(){
       this.fetchdata()
    }
    ModifyString()
    {
        const {userData, updatedValues} = this.state
        if(Object.keys(updatedValues).length)
        {
            return(
                <img style={customStyles.imagestyle} src={`${BaseURL}/${updatedValues.profile_img}`} alt="Profile" />
            )
        }
        else if(userData.profile_img)
        {
            var image = userData.profile_img.split(' ');
            var new_string = image.join('%20');
            return(
            <img style={customStyles.imagestyle} src={`${BaseURL}/${new_string}`} alt="Profile" />
        )
        }
        else{
            return(
                <img style={customStyles.imagestyle} src={require('../images/profile.png')} alt="Profile" />
            )
        }
    }
    setUser =(firstName, profileimg) => {
        console.log('val', firstName, '/', profileimg)
        this.setState({
            updatedValues:{
                first_name:firstName,
                profile_img:profileimg
            }
        })
    }
    project(str) {
        const {userData} = this.state
        switch (str) {
            // cb={this.successcb()}
            case "Profile": return <Profile data={userData} cbFn={()=> this.setUser}/>;
            case "Orders": return <Order/>;
            case "Address": return <Address/>;
            default: return (<Profile data={userData} cbFn={this.setUser}/>)
        }
    }

    checkUser(){
        
        const {userData,drawercontent,updatedValues} = this.state
        console.log(userData.user_img,'@@@@@@@@@@@@@@@@@@@22')
        return Object.keys(userData).length && (
            <div>
                <Grid style={customStyles.maindiv} container>
                    <Grid style={customStyles.headingdiv} item xs={12} >
                        My Account 
                        <hr/>
                    </Grid>
                    <Grid item xs={4}  align="center">
                        <StyleRoot>
                            <div style={styles.bounce}>
                                <Grid item xs={12}  align="center">
                                    {this.ModifyString()}
                                    {/* <img style={customStyles.imagestyle}  src={`${userData.user_img}`} alt="Profile"/> */}
                                </Grid>
                            </div>
                        </StyleRoot>
                        <Grid style={customStyles.namediv} item xs={12}  align="center">
                            {Object.keys(updatedValues).length ?
                                updatedValues.first_name :
                                userData.first_name
                            }
                        </Grid>
                        <Grid item xs={12}  align="center">
                            <Button onClick={() => this.setState({ drawercontent: 'Profile' })} style={customStyles.btn}> <AccountBoxIcon/>Profile</Button>
                        </Grid>
                        <Grid item xs={12}  align="center">
                            <Button onClick={() => this.setState({ drawercontent: 'Orders' })} style={customStyles.btn}> <ReorderIcon/>Orders</Button>
                        </Grid>
                        <Grid item xs={12}  align="center">
                            <Button onClick={() => this.setState({ drawercontent: 'Address' })} style={customStyles.btn}> <ReorderIcon/>Address</Button>
                        </Grid>
                    </Grid>
                    <Grid item xs={8}>
                        <Paper style={customStyles.userdetails}>
                            {this.project(drawercontent)}
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        )
    }
    userNotValid(){
        console.log('function',this.props)
        toastError("Login First")
        // setTimeout(() => 
        this.props.history.push('/dashboard',{flag:true})
        // ,10)
    }
    render() {
        
        const {authenticated} = this.state
        console.log(authenticated)
        return(
                authenticated 
                ?
                    this.checkUser()
                :
                    this.userNotValid()
        )
        
    }
}

export default UserDrawer
