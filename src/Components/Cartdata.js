import CryptoJS from 'crypto-js'
export default function Cartdata() {
    var checkdata = window.localStorage.getItem('cartdata')
    if(checkdata){
        var bytes=CryptoJS.AES.decrypt(window.localStorage.getItem('cartdata'), 'secret key 123');
        var cart=JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        return {cart}
    }
    return false
}
