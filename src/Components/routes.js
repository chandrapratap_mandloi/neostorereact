import React from 'react';
import Dashboard from './Dashboard'
import ProductDesc from './ProductDesc'
import { Route, BrowserRouter as Router, Switch, Redirect } from 'react-router-dom'
import AllProduct from './AllProduct'
import Search from './Search'
import IntegrationAutosuggest from './IntegrationAutosuggest'
export const Routing = (
  // <div>
    <Router>
    <Switch>
          <Redirect exact from="/" to="dashboard" />
          <Route path="/dashboard" component={Dashboard} />
          <Route path="/product/:id?" component={ProductDesc} />
          <Route path="/productpage/:id?/:color_id?" component={AllProduct}/>
          {/* <Route path="/productpage/:id?" component={AllProduct}/> */}
          <Route path="/try" component={Search}/>
          <Route path="/search" component={IntegrationAutosuggest}/>
        </Switch>
    </Router>
)
