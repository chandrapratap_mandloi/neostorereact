import React, { Component } from 'react'
import Modal from 'react-modal';
import Grid from '@material-ui/core/Grid';
import BaseURL from './Baseurl';
import axios from 'axios'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Decrypt from './Userdata'
import CloseIcon from '@material-ui/icons/Close'
const customStyles = {
    formdiv:{
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center'
      },
      maindiv:{
        display: 'flex',
        justifyContent: 'center',
        padding: '2% 2% 4% 2%'
      },
    textfield:{
      width:'30%',
      marginBottom:'2%',
      height:40,
      borderRadius:'4%',
      paddingLeft:'2%'
    },
    paper:{
      width:'85%',
      paddingTop:'2%',
      paddingBottom:'2%'
    },
    errordiv:{
      fontSize:12,
      color:'red'
    },
    submitbtn:{
      width: '54%',
      height: 30,
      backgroundColor:'#3f51b5',
      color:'white',
      border:'none'
  },
  radiodiv:{
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent:'center'
    },
    addressbtn:{ backgroundColor: '#3f51b5', color: 'white' },
    paperdiv:{width: '80%',
        paddingBottom: '2%'},
    themebtn:{marginRight: '5%'},
  };
  const modalStyle={content: {
    // backgroundColor: '#ff7f50',
    // backgroundColor: '#1d89e4',
    top: '54%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width:'50%'
  },}
class AlertDialog extends Component {
    constructor(props) {
        super(props)
        console.log(props)
        this.state = {
            userDetails: Decrypt().user,
            modalIsOpen: props.isVisible,
            data:props.address,
            fields: {},
            errors: {},
            address:'',
            pincode:'',
            city:'',
            state:'',
            country:''
        }
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }
    openModal() {
        this.setState({ modalIsOpen: this.props.isVisible });
      }
    
      afterOpenModal() {
        // this.subtitle.style.color = '#f00';
      }
    
      closeModal() {
          console.log('inside close modal',this.props)
          this.props.cbClose(false, false)
        // this.props.cbClose(false)
      }
      componentDidMount(){
        window.scrollTo(0, 0);
        Modal.setAppElement('body');
      }

      handlechange=(event,type)=>{
        const {data} = this.state
        var obj={...data}
        if(type==='address')
        {
            obj.address=event.target.value
        }
        else if(type==='pincode')
        {
            obj.pincode=event.target.value
        }
        else if(type==='city')
        {
            obj.city=event.target.value
        }
        else if(type==='state')
        {
            obj.state=event.target.value
        }
        else if(type==='country')
        {
            obj.country=event.target.value
        }
        this.setState({ data: obj },()=>console.log(this.state.data));
    }
    //   handlechange=(event,type)=>{
    //     console.log(event,type)
    //     if(type==='address')
    //     {
    //         this.setState({address:event.target.value})
    //     }
    //     else if(type==='pincode')
    //     {
    //         this.setState({pincode:event.target.value})
    //     }
    //     else if(type==='city')
    //     {
    //         this.setState({city:event.target.value})
    //     }
    //     else if(type==='state')
    //     {
    //         this.setState({state:event.target.value})
    //     }
    //     else if(type==='country')
    //     {
    //         this.setState({country:event.target.value})
    //     }
    // }
    handleValidation=()=>{
        let errors = {};
        var address = document.getElementById('address').value;
        var pincode = document.getElementById('pincode').value; 
        var city = document.getElementById('city').value;   
        var state = document.getElementById('state').value;   
        var country = document.getElementById('country').value;     
        let formIsValid = true;

        //Email
        if(address.length<10){
           formIsValid = false;
           errors["address"] = "Enter 10 Characters";
        }
        else if(pincode.length<6){
            formIsValid = false;
            errors["pincode"] = "Pincode must be of 6 digits";
         }
         else if(pincode.length>6){
            formIsValid = false;
            errors["pincode"] = "Only 6 Characters Allowed";
         }
         else if(city.length<3){
            formIsValid = false;
            errors["city"] = "City must be three characters long";
         }
         else if(state.length<3){
            formIsValid = false;
            errors["state"] = "State must be three characters long";
         }
         else if(country.length<3){
            formIsValid = false;
            errors["country"] = "Country must be three characters long";
         }
         else
         {
            //  console.log('else')
             formIsValid = true;
         }
        
        
       this.setState({errors: errors},()=> console.log(this.state.errors));
       return formIsValid;
   }
   handleSubmit = (event) => {
        event.preventDefault()
        const {userDetails,data} = this.state
        console.log('handle: ', this.handleValidation())
        const config = {
            headers: {
                'Authorization':`${userDetails.token}`
            }
        }
        const address={
            "address_id":data._id,
            "address": data.address,
            "pincode": data.pincode,
            "city":data.city,
            "state":data.state,
            "country":data.country
        }
        console.log(address)
        if(this.handleValidation()){
            axios.post(`${BaseURL}/editaddress`,address,config)
            .then(response =>{
                if(response.data.success)
                {
                    console.log(response.data)
                    this.props.cbClose(false, response.data.success)
                }
            })
            .catch(error => {console.log(error,'error')})
        }
        else{
            alert("Form has errors")
        }
    }
    render() {
        console.log('********************',this.state.modalIsOpen)
        console.log(this.props)
        const {data} = this.state
        return (
            <div>
                <Modal
                    isOpen={this.props.isVisible}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={modalStyle}
                    contentLabel="Example Modal"
                >
                    <CloseIcon onClick={this.closeModal} />
                    <Grid container>
                    <form style={customStyles.formdiv} onSubmit={this.handleSubmit}> 
                        <Grid item xs={12} align="center">
                            <TextField
                                required
                                id="address"
                                label="Address"
                                value={data.address}
                                autoComplete='address-line1'
                                className={customStyles.textfield}
                                onChange={()=>this.handlechange(event,'address')}
                                margin="normal"
                            />
                            <br/>
                            <span style={{color: "red",fontSize:12}}>
                                <b>{this.state.errors["address"]}</b>
                            </span>
                        </Grid>
                        <Grid item xs={12} align="center">
                            <TextField
                                required
                                id="pincode"
                                label="Pincode"
                                value={data.pincode}
                                type="number"
                                autoComplete='postal-code'
                                className={customStyles.textfield}
                                onChange={()=>this.handlechange(event,'pincode')}
                                margin="normal"
                            />
                            <br/>
                            <span style={{color: "red",fontSize:12}}>
                                <b>{this.state.errors["pincode"]}</b>
                            </span>
                        </Grid>
                        {/* <Grid container> */}
                            <Grid item xs={12} align="center">
                                <TextField
                                    required
                                    id="city"
                                    label="City"
                                    value={data.city}
                                    autoComplete='address-level2'
                                    className={customStyles.textfield}
                                    onChange={()=>this.handlechange(event,'city')}
                                    margin="normal"
                                />
                                <br/>
                                <span style={{color: "red",fontSize:12}}>
                                    <b>{this.state.errors["city"]}</b>
                                </span>
                            </Grid>
                            <Grid item xs={12} align="center">
                                <TextField
                                    required
                                    id="state"
                                    label="State"
                                    value={data.state}
                                    autoComplete='address-level1'
                                    className={customStyles.textfield}
                                    onChange={()=>this.handlechange(event,'state')}
                                    margin="normal"
                                />
                                <br/>
                                <span style={{color: "red",fontSize:12}}>
                                    <b>{this.state.errors["state"]}</b>
                                </span>
                            </Grid>
                        {/* </Grid> */}
                        <Grid item xs={12} align="center">
                            <TextField
                                required
                                id="country"
                                label="Country"
                                value={data.country}
                                autoComplete='country-name'
                                className={customStyles.textfield}
                                onChange={()=>this.handlechange(event,'country')}
                                margin="normal"
                            />
                            <br/>
                            <span style={{color: "red",fontSize:12}}>
                                <b>{this.state.errors["country"]}</b>
                            </span>
                        </Grid>
                            <br/>
                        <Grid item xs={12} align="center">
                            <Button type='submit' 
                                style={customStyles.themebtn} 
                                variant="outlined" 
                                color="primary"
                            >
                                Add Address
                            </Button>
                            <Button type='button' 
                                onClick={()=>this.props.cbFn(false)} 
                                variant="outlined" 
                                color="primary" 
                                style={customStyles.themebtn}
                            >
                               Cancel 
                            </Button>
                        </Grid>
                        </form>
                    </Grid>
                </Modal>
            </div>
        )
    }
}

export default AlertDialog
