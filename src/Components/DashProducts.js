import React, { Component } from 'react'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import BaseURL from './Baseurl'
import axios from 'axios'
import Grid from '@material-ui/core/Grid';
import Rating from '@material-ui/lab/Rating';
// import ProductDesc from './ProductDesc'
const customStyles = {
    subdiv: {
      paddingTop:'1%',
      paddingLeft:'8%',
      paddingRight:'8%',
      paddingBottom:'1%'
    },
    img:{
        // height: 150,
        // width: '100%',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 5,
        display: 'block',
        maxWidth:230,
        maxHeight:95,
        width: 'auto',
        height: 'auto'
    },
    card:{
        maxWidth:450,
        height:260,
        width:300, 
        paddingTop:'4%', 
        paddingLeft:'1%', 
        paddingRight:'1%', 
        paddingBottom:'1%'
    }
  };
  
class DashProducts extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            dash:[],
            rating:0
        }
    }

    componentDidMount(){
        window.scrollTo(0, 0);
        axios.get(`${BaseURL}/showproducts`)
            .then(response => {
                this.setState({
                    dash:response.data.customer
                })

            })
            .catch(error => { console.log(error, 'error') })
    }
    showProduct(step){
        console.log(step,'clicked card')
        this.props.history.push({
            pathname:`product/${step.products.product_id}`,
            state: {step}
          }) 
    }
    render() {
        // const classes = useStyles();
        const {dash} = this.state
        return (
            <div>
                <Grid style={customStyles.subdiv} container spacing={3}>
                 {dash.map((step, index) =>  
                 <Grid item xs={12} sm={4} md={4} key={index} style={{display:'flex', justifyContent:'center'}}>
                <Card onClick={()=>this.showProduct(step)}  key={step.products.product_id} style={customStyles.card}>
                    <CardActionArea>
                        <CardMedia
                        component="img"
                        alt="Contemplative Reptile"
                        // height="300"
                        style={customStyles.img}
                        image={`${BaseURL}/${step.products.product_image[0]}`}
                        title="Contemplative Reptile"
                        />
                        <CardContent style={{textAlign:'center'}}>
                        <Typography gutterBottom variant="h5" component="h2">
                            {step.products.product_name}
                        </Typography>
                        <Typography style={customStyles.costdiv} gutterBottom variant="h5" component="h2">
                            {step.products.product_cost.toLocaleString("en-IN")} ₹
                        </Typography>
                        <Typography style={customStyles.stardiv} gutterBottom variant="h5" component="h2">
                            <Rating name="half-rating" value={step.products.product_rating} precision={0.5} readOnly/>
                        </Typography>
                        
                        </CardContent>
                    </CardActionArea>
                    </Card>
                    </Grid>
                )}
                </Grid>
            </div>
        )
    }
}

export default DashProducts
