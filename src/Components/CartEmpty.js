import React, { Component } from 'react'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
// import ProductDesc from './ProductDesc'
const customStyles = {
    subdiv: {
        display:'flex',
        justifyContent:'center'
    },
    img:{
        height:220,
        width:220
    },
    card:{
        // height:280,
        width:'51%',
        paddingTop:'1%',
        paddingLeft:'1%',
        paddingRight:'1%',
        paddingBottom:'1%'
    },
    maindiv:{
        display:'flex',
        justifyContent:'center',
        paddingBottom:'2%'
    },

  };
  
class CartEmpty extends Component {
    constructor(props) {
        super(props)

        this.state = {
            
        }
    }
    redirectToDashboard(){
        console.log(this.props,'======')
        this.props.history.push('/dashboard')
    }
    render() {
        return (
            <div style={customStyles.maindiv}>
                <Card style={customStyles.card}>
                <div style={customStyles.subdiv}>
                        <img style={customStyles.img} src={require('../images/cart.png')} alt="Profile" />
                        </div>
                        <CardContent style={{textAlign:'center'}}>
                        <Typography  component="h2">
                            Before proceed to checkout you must add some products to you shopping cart.
                        </Typography>
                        <Typography  component="h2">
                            You will find lots of intresting products on our products page
                        </Typography>
                        
                        </CardContent>
                    <CardActionArea onClick={()=>this.redirectToDashboard()}>
                        <Typography style={customStyles.subdiv} component="h2">
                            Return To HomePage
                        </Typography>
                    </CardActionArea>
                </Card>
            </div>
        )
    }
}

export default CartEmpty
