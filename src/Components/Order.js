import React, { Component } from 'react'
import Decrypt from './Userdata'
import axios from 'axios'
import Grid from '@material-ui/core/Grid';
import BaseURL from './Baseurl'
import Paper from '@material-ui/core/Paper';
import moment from 'moment'
import Button from '@material-ui/core/Button';

const customStyles={
    innerdiv:{
        display:'flex',
    },
    ordersdiv:{
        display:'flex',
        justifyContent:'center',
        marginBottom:'2%',
        
    },
    img: {
        // height: 150,
        // width: '100%',
        // marginLeft: 'auto',
        // marginRight: 'auto',
        // marginTop: 5,
        // alignItem: 'center',
        // display: 'block',
        // maxWidth:230,
        // maxHeight:95,
        // width: 'auto',
        // height: 'auto'
        width:100,
        height:80,
        border:'1px solid',
        cursor: 'pointer',

    },
    maindiv:{
        paddingTop:'2%',
        paddingBottom:'2%'
    },
    textStyle:{
        color: '#CB7C13',
        fontWeight: 600
    },
    textStyles:{
        color: '#CB7C13',
        fontWeight: 600,
        paddingLeft:'4%'
    }
}
export class Order extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            userDetails: Decrypt().user,
            order:[]
        }
    }
    
    componentDidMount(){
        window.scrollTo(0, 0);
        const {userDetails} = this.state
        axios.get(`${BaseURL}/order`, { headers: { Authorization: userDetails.token } })
            .then(response => {
                if(response.data.success)
                {
                    console.log(response.data)
                    this.setState({
                        order:response.data.customer
                    })
                }
            })
            .catch((error) => {
                console.log('error ', error)
            })
    }
    downloadInvoice(order){
        console.log(order,'========')
        const { userDetails } = this.state
            const config = {
                headers: {
                    'Authorization':`${userDetails.token}`
                }
              }
              const data = {
                order:order._id
              }
                axios.post(`${BaseURL}/invoice`,data,config)
                //  axios.get(`${BaseURL}/invoice/${order._id}`,config)
                .then(response =>{
                    if(response.data.success)
                    {
                      console.log('success',response.data)  
                      window.open(`${response.data.receipt}`, "_blank")
                    }
                })
                .catch(error => {console.log(error,'error')})
    }
    showOrderedProducts(){
        const {order} = this.state
        return(
            <div style={customStyles.maindiv}>
                    {order.map((order,index) => (
                        <div key={index} style={customStyles.ordersdiv}>
                        <Paper style={{width:'80%',padding:'2% 2% 2% 2%'}}>
                            <Grid container>
                                <Grid style={customStyles.innerdiv} item xs={12}>
                                    <Grid style={customStyles.textStyle} item xs={2}>
                                        TRANSIT
                                    </Grid>
                                    <Grid item xs={8} style={{display:'flex'}}>
                                        <b>
                                            Order No:
                                        </b>
                                        {order._id}
                                    </Grid>
                                </Grid>
                                <Grid style={customStyles.innerdiv} item xs={12}>
                                    <Grid item xs={2}>
                                        Placed On
                                    </Grid>
                                    <Grid item xs={8} style={{display:'flex'}}>
                                        {moment(order.products[0].orderplaced).format('Do MMM YY')}
                                        <div style={customStyles.textStyles}>
                                            ₹{order.products[0].ordertotal}
                                        </div>
                                    </Grid>
                                </Grid>
                                <Grid  container>
                                    {order.products.map((images,index) => (
                                        <Grid key={index} item xs={3}> 
                                            <img 
                                                style={customStyles.img} 
                                                data-toggle="tooltip"
                                                data-placement="left" 
                                                title={images.orders[0].product_name}
                                                src={`${BaseURL}/${images.orders[0].product_image[0]}`} 
                                                alt={images.product_name} 
                                            />
                                        </Grid>
                                    ))}
                                </Grid>
                                
                                <Grid item xs={12}>
                                <hr/>
                                <Button 
                                    onClick={()=>this.downloadInvoice(order)} 
                                    style={{ backgroundColor: '#3f51b5', color: 'white' }}
                                >
                                    Download invoice as PDF 
                                </Button>
                                </Grid>
                            </Grid>
                        </Paper>
                        </div>
                    ))}
            </div>
        )
    }
    showNoOrderFound(){
        return(
            <div>
                No Order
            </div>
        )
    }
    render() {
        const {order} = this.state
        return (
            <div>
                {order.length===0 ? this.showNoOrderFound() : this.showOrderedProducts()}
            </div>
        )
    }
}

export default Order

// <Grid item xs={12}>
//                         <div>
//                             <h3>TRANSIT</h3>
//                         </div>
//                         <div>

//                         </div>
//                     </Grid>