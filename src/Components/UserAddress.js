import CryptoJS from 'crypto-js'
export default function UserAddress() {
    var checkdata = window.localStorage.getItem('address')
    if(checkdata){
        var bytes=CryptoJS.AES.decrypt(window.localStorage.getItem('address'), 'secret key 123');
        var address=JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        return {address}
    }
    return false
}
