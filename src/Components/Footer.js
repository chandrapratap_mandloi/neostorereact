import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
const customStyles = {
    footertext: {
        color:'white'
    },
    about:{
        textAlign:'center'
    },
    ul:{
        listStyleType: 'none'
    },
    li:{
        paddingBottom:'2%'
    },
    button:{fontSize:10},
    emailfield:{width:220, height:40,backgroundColor:'white'},
  };
class Footer extends Component {
    render() {
        return (
            <div style={{backgroundColor:'#3f51b5',flex:0.2}}>
                <Grid container style={customStyles.footertext} spacing={3}>
                    <Grid style={customStyles.about} item xs={4}>
                        <h2>About Company</h2>
                        <div>
                            <p>NeoSOFT Technologies is here at your quick <br/> and easy service for shooping .</p>
                            <h4>Contact information</h4>
                            <p>Email: contact@neosofttech.com</p>
                            <p>Phone: +91 0000000000</p>
                            <p>MUMBAI, INDIA</p>
                        </div>
                    </Grid>
                    <Grid style={customStyles.about} item xs={4}>
                        <h2>Information</h2>
                        <ul style={customStyles.ul}>
                            <li style={customStyles.li}>Terms &amp; Conditions</li>
                            <li style={customStyles.li}>Guarantee &amp; Return Policy</li>
                            <li style={customStyles.li}>Contact us</li>
                            <li style={customStyles.li}>Privacy Policy</li>
                            <li style={customStyles.li}>Locate Us</li>
                        </ul>
                    </Grid>
                    <Grid style={customStyles.about} item xs={4}>
                        <h2>Newsletter</h2>
                        <p>Sign up to get exclusive offers from our favorite <br/> brands and to be well up in the news.</p>
                        <TextField
                            id="outlined-search-input"
                            type="Your Email"
                            name="Your Email"
                            autoComplete="false"
                            margin="normal"
                            variant="outlined"
                            placeholder="Your Email"
                            style={customStyles.emailfield}
                        />
                        <Grid style={customStyles.about} item xs={12}>
                            <Button style={customStyles.button} variant="contained" >
                                Subscribe
                            </Button>
                        </Grid>
                    </Grid>
                    <Grid style={customStyles.about} item xs={12}>
                    <p>Copyright © 2017 NeoSOFT Technologies All rights reserved | Design by NeoSOFT Technologies</p>
                        </Grid>
                   
                </Grid>
            </div>
        )
    }
}

export default Footer
