import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import CardTravelIcon from '@material-ui/icons/CardTravel';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import SearchIcon from '@material-ui/icons/Search';
import AddShoppingCartIcons from '@material-ui/icons/AddShoppingCart';
import UserChoice from './UserChoice'
import {DebounceInput} from 'react-debounce-input';
import BaseURL from './Baseurl'
import axios from 'axios'
import Isauth from './Isauth'
import NotificationBadge from 'react-notification-badge';
import {Effect} from 'react-notification-badge';
// import CartButton from './Cartbutton'
import { connect } from 'react-redux';
import { addToCart, fetchData } from './Redux/actions/cartActions';
import { Redirect } from 'react-router-dom'
import { withRouter } from 'react-router-dom'
import { bounceInLeft } from 'react-animations';
import Radium, {StyleRoot} from 'radium';

const styles = {
    bounce: {
    animation: 'x 1s',
    animationName: Radium.keyframes(bounceInLeft, 'bounceInLeft')
  }
}

// import { Redirect } from 'react-router-dom'
const customStyles={
    maindiv:{
        paddingTop:'5%',
        paddingLeft:'5%'
    },
    inputfield:{
        width:380, 
        backgroundColor:'white',
        height:30,
        border:'none'
    },
    subdiv:{
        width: 300,
        alignItems: 'center',
        border: '1px solid',
        display: 'flex'
    },
    allproductbtn: {
        backgroundColor: '#f4f4f4',
        color: '#444',
        cursor: 'pointer',
        padding: 6,
        width: '90%',
        textAlign: 'left',
        // border: 'none'
        marginBottom:'1%',
        border:'1px solid'
    },
    ulStyle:{
        margin:0,
        listStyleType: 'none',
        display:'flex',
        flexDirection:'column',
        width:'104%',
        backgroundColor:'white',
        maxHeight:0,
    },
    homebtn:{
        color: 'white',
        display: 'flex',
        justifyContent: 'space-around',
        width: '102%',
        fontSize:20,
    }
}
class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {
            count:5,
            value: '',
            products:[],
            link:false,
            authenticated:Isauth()
        }
    }
    
    redirectdashboard(){
        console.log('in func to dash')
        return <Redirect to='/dashboard'/>
    }
    handleSearch = (event) => {
        console.log(event.target.value)
        // suggestion
        // this.setState({
        //     value: event.target.value
        // })
        this.setState({
            value: event.target.value
        }, () =>
            axios.get(`${BaseURL}/suggestion?text=${this.state.value}`)
                .then(response => {
                    console.log(response.data)
                    this.setState({
                        products:response.data.product
                    })
                })
                .catch(error => { console.log(error, 'error') })
        )
    }
    searchProduct(step){
        console.log(step)
        this.props.history.push({
            pathname:`/product/${step._id}`,
            state: {step}
          }) 
    }
    render() {
        const {products } = this.state
        return (
            <React.Fragment>
                <CssBaseline />
                    <AppBar style={{height:65, flex:0.2}}>
                    <Grid container spacing={3}>
                        <Grid item xs={3} style={{display:'flex', justifyContent:'center'}}>
                        <Toolbar>
                        <StyleRoot>
                        <div style={styles.bounce}>
                        <Button 
                            onClick={()=>this.props.history.push('/dashboard')}
                            style={customStyles.homebtn}
                        >
                            NeoStore
                            <CardTravelIcon />
                        </Button>
                        </div>
                        </StyleRoot>
                        {/* <StyleRoot>
                        <div style={styles.bounce}>STYLEEEE</div></StyleRoot> */}
                        </Toolbar>
                        </Grid>
                        <Grid  item xs={3}>
                        <div style={customStyles.maindiv}>
                <div style={customStyles.subdiv}>
                <DebounceInput
                    minLength={2}
                    debounceTimeout={300}
                    placeholder="Search"
                    style={customStyles.inputfield}
                    // onChange={event => this.setState({value: event.target.value})} 
                    onChange={this.handleSearch}
                />
                <SearchIcon/>
                </div>
                <ul style={customStyles.ulStyle}>
                    {products.map((step, index) =>  
                    // this.props.history.push({
                    //     pathname: `product/${step._id}`,
                    //     state: { step }
                    // })
                    //     <a style={{color:'white',textDecoration:'none'}} href="/dashboard">
                    //     <Button onClick={()=>this.searchProduct(step)} style={customStyles.allproductbtn}>
                    //     <li>{step.product_name}</li>
                    //     </Button>
                    // </a>
                    <Button onClick={()=>this.searchProduct(step,this.props)} style={customStyles.allproductbtn}>
                        <li>{step.product_name}</li>
                    </Button>
                        
                    )}
                </ul>
            </div>
                        </Grid>
                        <Grid item xs={2} style={{display:'flex', justifyContent:'center'}}>
                        <Toolbar>
                        <Button style={{color:'white'}}
                            onClick={()=>this.props.history.push('/cart')}
                        >
                        <AddShoppingCartIcons />
                            Cart
                        </Button>
                        <NotificationBadge style={{top:10,backgroundColor:'#3f51b5'}} count={this.props.cartQty} effect={Effect.SCALE}/>
                            <Button 
                                style={{color:'white'}}
                                onClick={()=>this.props.fetchData()}
                            >
                                Cart
                                {console.log(this.props.apidata,'======')}
                                {/* <CartButton/> */}
                            </Button>
                        </Toolbar>
                        </Grid>
                        <Grid item xs={4} style={{display:'flex', justifyContent:'center'}}>
                            <Toolbar>
                               <UserChoice {...this.props}/>
                            </Toolbar>
                        </Grid>
                    </Grid>
                    </AppBar>
                    <Toolbar id="back-to-top-anchor" />
                    <Container>
                    <Box my={2}>
                    
                    </Box>
                    </Container>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => (
    {
    cartQty: state.pReducer.cartQty,
    apidata: state.pReducer.apidata
});

const mapDispatchToProps = dispatch => ({
    addToCart: () => dispatch(addToCart()),
    fetchData: () => dispatch(fetchData())
});
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header))
