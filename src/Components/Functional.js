import React, { useState,useEffect } from 'react';
function Functional() {
    const [count, setCount] = useState(10);
    // const [count, decreaseCount] = useState(10);

    // Similar to componentDidMount and componentDidUpdate:
    useEffect(() => {
      // Update the document title using the browser API
      document.title = `You clicked ${count} times`;
    });
  return (
    <div>
      <p>You clicked {count} times</p>
      <button style={{height:40}} onClick={() => setCount(count + 1)}>
        Click me
      </button>
      <button style={{height:40}} onClick={() => setCount(count - 1)}>
        Don't Click me
      </button>
    </div>
  );
}

export default Functional
