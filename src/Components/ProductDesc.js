import React, { Component } from 'react'
import Paper from '@material-ui/core/Paper';
import BaseURL from './Baseurl'
import axios from 'axios'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Rating from '@material-ui/lab/Rating';
import ShareIcon from '@material-ui/icons/Share'
import TabPanel from './Tabpanel'
import ImageZoom from 'react-medium-image-zoom'

import 
    {  
        //  TwitterCount, 
        TwitterButton, 
        // LinkedInCount, 
        LinkedInButton,
        // FacebookCount,
        FacebookButton,
        // GooglePlusCount,
        // GooglePlusButton,
        EmailButton
    } from "react-social";
import RateProduct from './RateProduct'
import {
    FacebookShareCount,
    EmailIcon,
    FacebookIcon,
    // WhatsappIcon,
    LinkedinIcon,
    TwitterIcon,
    TwitterShareCount,
    // FacebookShareButton
   
  } from 'react-share';

import Button from '@material-ui/core/Button';
// import {zoomIn} from 'react-animations'
// import Radium, {StyleRoot} from 'radium';

// const styles = {
//     bounce: {
//     animation: 'x 1s',
//     animationName: Radium.keyframes(zoomIn, 'zoomIn')
//   }
// }
const customStyles = {
    maindiv:{
        display:'flex',
        justifyContent:'center',
        padding:'2% 2% 2% 2%'
    },
    img: {
      height:200,
      width:350
    },
    subimg:{
        height:50,
        width:100,
        marginRight:'2%',
        cursor: 'pointer'
    },
    imgdiv:{
        display:'flex',
        justifyContent:'center',
        marginBottom:'2%'
    },
    socialimg:{height:25},
    socialbtn:{ marginRight:'2%', border:'none', backgroundColor:'#eeeeee'},
    button:{
        backgroundColor:'#3f51b5',
        color:'white',
        marginRight:'5%'
    },
    subimgdiv:{
        display:'flex',
        justifyContent:'center'
    },
    maingrid:{
        paddingTop:'2%'
    }
  };

  class ProductDesc extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             data:'',
             imageindx:0,
             modal: false
        }
    }
    componentDidMount() {
        window.scrollTo(0, 0);
        axios.get(`${BaseURL}/findproducts?_id=${this.props.match.params.id}`)
            .then(response => {
                this.setState({
                    data:response.data.product
                })

            })
            .catch(error => { console.log(error, 'error') })
    }
    shareFacebook(){
        const url = 'http://localhost:34708/product/5d0256d3d023f411afebcf9bm'
        return <FacebookShareCount url={url} />
        
    }
    shareTwitter(){
        const url = 'http://localhost:34708/product/5d0256d3d023f411afebcf9bm'
        return  <TwitterShareCount url={url}>
       
      </TwitterShareCount>
    }
    modalclose = (value, isUpdated) => {
        this.setState({
            modal: value
        })
        if (isUpdated) {
            this.fetchData()
        }
    }
    render() {

        const {data} = this.state
        return data.length>0 && ( 
            <div style={customStyles.maindiv}>
                {/* <TwitterShareCount url={url}></TwitterShareCount> */}
                {/* {data.length>0 && console.log(data[0])} */}
               <Paper style={{width:'90%', backgroundColor:'#eeeeee'}}>
                    <Grid container style={customStyles.maingrid} spacing={3}>
                        <Grid item xs={6} >
                            <div style={customStyles.imgdiv}>
                            <ImageZoom
                                image={{
                                    src: `${BaseURL}/${data[0].product_image[this.state.imageindx]}`,
                                    alt: 'Golden Gate Bridge',
                                    className: 'img',
                                    style: { height:200,
                                        width:350 }
                                }}
                                zoomImage={{
                                    src: 'bridge-big.jpg',
                                    alt: 'Golden Gate Bridge'
                                }}
                            />
                            </div>
                            <div style={customStyles.subimgdiv}>
                            {data[0].product_image.map((step, index) =>  
                                <img
                                    key={index} 
                                    onClick={()=>this.setState({imageindx:index})} 
                                    style={{...customStyles.subimg, border: index === this.state.imageindx && '1px solid'}} 
                                    src={`${BaseURL}/${step}`} 
                                    alt={data.product_name}
                                />
                            )}
                            </div>
                        </Grid>
                        <Grid item xs={6} >
                            <Typography variant="h5" component="h3">
                                {data[0].product_name}
                            </Typography>
                            <Typography component="p">
                                <Rating name="half-rating" value={data[0].product_rating} precision={0.5} readOnly/>
                            </Typography>
                            <hr/>
                            <Typography variant="h5" component="h3">
                               Price: {data[0].product_cost.toLocaleString("en-IN")} ₹
                            </Typography>
                            <Typography variant="h5" component="h3">
                                Color: 
                                <button  style={{backgroundColor:`${data[0].color_id.color_code}`,height:22,width:22,border: 'none'}}/>
                            </Typography>
                            <Typography variant="h5" component="h3">
                               Share on <ShareIcon/>
                            </Typography>
                            <Typography style={{marginBottom:'2%'}} variant="h5" component="h3">
                            <FacebookButton appId={382148426057043} url={`${BaseURL}/${this.props.location.pathname}`}  style={customStyles.socialbtn} >
                                <FacebookIcon size={32} round={true} />
                            </FacebookButton>
                            {/* EmailButton */}
                            <EmailButton url={`${BaseURL}/${this.props.location.pathname}`}  style={customStyles.socialbtn} >
                                <EmailIcon size={32} round={true} />
                            </EmailButton>
                            {/* <GooglePlusButton url={url} style={customStyles.socialbtn} >
                                <EmailIcon size={32} round={true} />
                            </GooglePlusButton> */}
                            <TwitterButton url={`${BaseURL}/${this.props.location.pathname}`}  style={customStyles.socialbtn} >
                                <TwitterIcon size={32} round={true} />
                            </TwitterButton>
                            <LinkedInButton url={`${BaseURL}/${this.props.location.pathname}`}   style={customStyles.socialbtn} >
                                <LinkedinIcon size={32} round={true} />
                            </LinkedInButton>
                            
                            {/* <Button style={customStyles.socialbtn} variant="contained" >
                                <WhatsappIcon size={32} round={true} />
                            </Button> */}
                            </Typography>
                            <Typography variant="h5" component="h3">
                                <Button variant="contained" style={customStyles.button}>
                                    Add to Cart
                                </Button>
                                 <RateProduct productdesc={data} img={data[0].product_image} cbClose={this.modalclose}  isVisible={this.state.modal} />
                                <Button onClick={()=>this.setState({modal:true})} variant="contained" style={customStyles.button} >
                                    Rate Product
                                </Button>
                            </Typography>
                        </Grid>
                        
                    </Grid>
                    <hr/>
                    {/* {data[0].product_cost} */}
                    <Grid container spacing={3}>
                        <Grid item xs={12} >
                        <TabPanel descrpition={data[0].prod_desc} dimension={data[0].product_dimension} material={data[0].product_material} producer={data[0].product_producer}/>
                        </Grid>
                       </Grid>
               </Paper>
            </div>
        )
    }
}

export default ProductDesc
