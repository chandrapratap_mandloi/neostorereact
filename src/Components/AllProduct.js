import React, { Component } from 'react'
import BaseURL from './Baseurl'
import { connect } from 'react-redux';
import axios from 'axios'
import CryptoJS from 'crypto-js'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import Decrypt from './Userdata'
import Cartdata from './Cartdata'
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// import DeleteIcon from '@material-ui/icons/Delete';
import Rating from '@material-ui/lab/Rating';
import Button from '@material-ui/core/Button';
import StarIcon from '@material-ui/icons/Star'
import ArrowDownward from '@material-ui/icons/ArrowDownward'
import ArrowUpward from '@material-ui/icons/ArrowUpward'
// import CustomizedExpansionPanels from './Accordian'
import Paper from '@material-ui/core/Paper';
import AccordionSide from './Accordion';
import { addToCart } from './Redux/actions/cartActions';
import { toastInfo,toastSuccess } from '../utils'; 
import { rotateIn } from 'react-animations';
// import {slideInLeft} from 'react-animations'
import Radium, {StyleRoot} from 'radium';
// import SpinnerPage from './Loader/GloabalLoader'
import Skeleton from '@material-ui/lab/Skeleton';


const styles = {
    bounce: {
    animation: 'x 1s',
    animationName: Radium.keyframes(rotateIn, 'rotateIn')
  }
}
const customStyles = {
    subdiv: {
        paddingTop: '1%',
        paddingLeft: '2%',
        paddingRight: '2%',
        paddingBottom: '1%'
    },
    img: {
        // height: 150,
        // width: '100%',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 5,
        alignItem: 'center',
        display: 'block',
        maxWidth:230,
        maxHeight:95,
        width: 'auto',
        height: 'auto'
    },
    card: {
        maxWidth: 450,
        height: 315,
        width:300, 
        paddingTop: '1%',
        paddingLeft: '1%',
        paddingRight: '1%',
        paddingBottom: '1%'
    },
    imgcard: {
        display: 'flex',
        justifyContent: 'center',
        marginBottom: '2%',
        // marginRight:'1%'
    },
    maindiv: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center'
    },
    sortbydiv:{
        display:'flex',
        justifyContent:'flex-end',
        paddingRight:'2%'
    },
    allproductbtn: {
        backgroundColor: '#f4f4f4',
        color: '#444',
        cursor: 'pointer',
        padding: 18,
        width: '100%',
        textAlign: 'left',
        border: 'none'
    },
    stardiv:{
        paddingTop:'3%'
    },
    productotfound:{
        // marginLeft:'-58%',
        textAlign:'center', 
        marginBottom:'4%'
    },
    cartBtn:{
        display: 'flex',
        justifyContent: 'center',
    },
    media: {
        height: 190,
    }
};
class AllProduct extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: [],
            userDetails: Decrypt().user,
            cartdata:Cartdata().cart,
            try:true,
            loader:true
        }
    }
    fetchProductList = () => {
        if(this.props.match.params.id && this.props.match.params.color_id){
            axios.get(`${BaseURL}/findproducts?category_id=${this.props.match.params.id}&color_id=${this.props.match.params.color_id}`)
                .then(response => {
                    this.setState({
                        data: response.data.product,
                        loader:true
                    })
                })
                .catch(error => { console.log(error, 'error') })
        }
        else if (this.props.match.params.id) {
            axios.get(`${BaseURL}/findproducts?category_id=${this.props.match.params.id}`)
                .then(response => {
                    this.setState({
                        data: response.data.product,
                        loader:true
                    })
                })
                .catch(error => { console.log(error, 'error') })
        }
        else if(this.props.match.params.color_id){
            alert('inside color id')
            axios.get(`${BaseURL}/findproducts?category_id=${null}&color_id=${this.props.match.params.color_id}`)
                .then(response => {
                    this.setState({
                        data: response.data.product,
                        loader:true
                    })
                })
                .catch(error => { console.log(error, 'error') })
        }
        else {
            this.handleallproducts()
            axios.get(`${BaseURL}/findproducts`)
                .then(response => {
                    this.setState({
                        data: response.data.product,
                        loader:true
                    })

                })
                .catch(error => { console.log(error, 'error') })
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.match.params.id !== prevProps.match.params.id) {
            this.fetchProductList();
        }
        if (this.props.match.params.color_id !== prevProps.match.params.color_id) {
            this.fetchProductList();
        }
    }
    handleallproducts() {
        this.props.history.push('/productpage')
    }
    componentDidMount() {
        window.scrollTo(0, 0);
        this.fetchProductList()
    }
    addtoCart(step){
        console.log(step,'cartdata*******')
        if(step.product_stock===0)
        {
            toastInfo("This Product is Out of Stock")
        }
        else{
            const { userDetails,cartdata } = this.state
            const config = {
                headers: {
                    'Authorization':`${userDetails.token}`
                }
              }
              const data = [{
                product_id:step._id,
                qty:1
              }]
              if(!cartdata.includes(step._id)){
                axios.post(`${BaseURL}/cart`,data,config)
                .then(response =>{
                    if(response.data.success)
                    {
                        this.props.addToCart();
                        cartdata.push(step._id)
                        window.localStorage.setItem('cartdata',CryptoJS.AES.encrypt(JSON.stringify(cartdata),'secret key 123'))
                        console.log(response.data)
                        this.setState({
                            flag:false
                        })
                        toastSuccess("Product Added to cart")  
                    }
                })
                .catch(error => {console.log(error,'error')})
            }
            else{
                // alert('Already in cart')
                toastInfo("Product Already in cart")
            }
        }    
    }
    showProduct(step) {
        this.props.history.push({
            pathname: `product/${step._id}`,
            state: { step }
        })
    }
    sortProduct(sort){
        if(this.props.match.params.id){
            axios.get(`${BaseURL}/findproducts?category_id=${this.props.match.params.id}&sortBy=${sort.sortBy}&desc=${sort.desc}`)
                .then(response => {
                    this.setState({
                        data: response.data.product
                    })
                })
        .catch(error => { console.log(error, 'error') })
        }
        else{
            axios.get(`${BaseURL}/findproducts?sortBy=${sort.sortBy}&desc=${sort.desc}`)
                .then(response => {
                    this.setState({
                        data: response.data.product
                    })
                })
        .catch(error => { console.log(error, 'error') })
        } 
    }
    showproductsacctofilter()
    {
        const { data } = this.state
        return(
            <div style={customStyles.maindiv}>
            {data.map((step, index) =>
                <Grid key={index} item xs={12} sm={6} md={4} style={customStyles.imgcard}>
                    <StyleRoot>
                    <div style={styles.bounce}>
                    <Card  key={step._id} style={customStyles.card}>
                        <CardActionArea onClick={() => this.showProduct(step)} key={step.index}>
                                    <CardMedia
                                        component="img"
                                        alt="Contemplative Reptile"
                                        style={customStyles.img}
                                        image={`${BaseURL}/${step.product_image[0]}`}
                                        title="Contemplative Reptile"
                                    />
                                
                            <CardContent style={{ textAlign: 'center' }} >
                                <Typography gutterBottom variant="h5" component="h2">
                                    {step.product_name}
                                </Typography>
                                <Typography gutterBottom variant="h5" component="h2">
                                    {step.product_cost.toLocaleString("en-IN")} ₹
                                </Typography>
                                <Typography style={customStyles.stardiv} gutterBottom variant="h5" component="h2">
                                    <Rating name="half-rating" value={step.product_rating} precision={0.5} readOnly />
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions style={customStyles.cartBtn}>
                            <Button onClick={()=>this.addtoCart(step)} style={{ backgroundColor: '#3f51b5', color: 'white' }}>
                                Add To Cart
                            </Button>
                        </CardActions>
                    </Card>
                    </div>
                            </StyleRoot>
                </Grid>
            )}
            </div>
        )
    }
    shownoproductfound(){
        const {loader} = this.state
        return (
            <div style={customStyles.maindiv}>
                <Paper style={customStyles.productotfound}>
                    {
                        loader ? <Skeleton animation="wave" variant="rect" style={customStyles.media}/>
                        : <img src={require('../images/notfound.jpeg')} alt="Product Not Found" /> 
                    }
                    <Button style={customStyles.allproductbtn} onClick={() => this.handleallproducts()}>
                            All Products
                    </Button>
                </Paper>
            </div>
        )
    }
    render() {
        const { data } = this.state
        console.log(this.props,'sdsdsdsmd')
        return  (
            <div>
                {/* <SpinnerPage visible={loader}/> */}
                <Grid style={customStyles.subdiv} container spacing={1}>
                    <Grid item xs={2} >
                        <Button style={customStyles.allproductbtn} onClick={() => this.handleallproducts()}>
                            All Products
                        </Button>
                        <AccordionSide {...this.props} />
                    </Grid>
                    <Grid item xs={10}  >
                    <Grid item xs={12} style={customStyles.sortbydiv} >
                        <Button style={{color:'black'}} disabled>
                            Sort By:
                        </Button>
                        <Button onClick={() => this.sortProduct({sortBy:'product_rating', desc:true})}>
                            <StarIcon/>
                        </Button>
                        <Button onClick={() => this.sortProduct({sortBy:'product_cost', desc:false})} >
                            ₹ <ArrowDownward/>
                        </Button>
                        <Button onClick={() => this.sortProduct({sortBy:'product_cost', desc:true})}>
                            ₹ <ArrowUpward/>
                        </Button>
                    </Grid>
                        {/* {this.showproductsacctofilter()} */}
                        {data.length===0 ? this.shownoproductfound():this.showproductsacctofilter()}
                    </Grid>
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    cartQty: state.pReducer.cartQty
});

const mapDispatchToProps = dispatch => ({
    addToCart: () => dispatch(addToCart())
});

export default connect(mapStateToProps, mapDispatchToProps)(AllProduct)
