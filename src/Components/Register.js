import React, { Component } from 'react'
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import BaseURL from './Baseurl';
import axios from 'axios'


const customStyles={
    heading:{
      fontSize: 34,
      color: 'red',
      paddingBottom: '2%',
    },
    maindiv:{
        height:'100%',
        paddingTop:'2%',
        paddingBottom:'4%'
    },
    textfield:{
      width:'38%',
      marginBottom:'2%',
      height:40,
      borderRadius:'4%',
      paddingLeft:'2%'
    },
    paper:{
      width:'85%',
      paddingTop:'2%',
      paddingBottom:'2%'
    },
    secondgrid:{
      padding: '2% 2% 2% 2%'
    },
    errordiv:{
      fontSize:12,
      color:'red'
    },
    socialbtn:{
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-around',
      backgroundColor:'#3b5998',
      color:'white',
      border: 0,
      borderRadius: 3,
      cursor: 'pointer',
      fontSize: 19,
      margin: 5,
      width: 'calc(100% - 10px)',
      overflow: 'hidden',
      padding: '0px 10px',
      userSelect: 'none',
      height: 50,
    },
    submitbtn:{
        width:'38%',
        height:40,
        backgroundColor:'#3f51b5',
        color:'white'
    },
    radiobtndiv:{
      display: 'flex',
      justifyContent: 'center',
      paddingBottom: '2%'
    },
    radio:{
      display: 'flex',
      justifyContent: 'center',
    }
  }
const SignupSchema = Yup.object().shape({
    first_name:Yup.string()
    .label('First Name')
    .required('Required')
    .min(3, 'Name must be 3 characters long'),
    last_name:Yup.string()
    .min(3, 'Last Name must be 3 characters long')
    .required('Required'),
    email: Yup.string()
      .email('Invalid email')
      .required('Required'),
    password: Yup.string()
      .min(4, 'Password Must be four characters long!')
      .max(20, 'Too Long!')
      .required('Required'),
    cnfpassword: Yup.string()
      .min(4, 'Password Must be four characters long!')
      .max(20, 'Too Long!')
      .required('Required')
      .test('passwords-match', 'Passwords must match ', function(value) {
        return this.parent.password === value;
      }),
    phone:Yup.string()
    .min(10, 'Minimum 10 characters long!')
    .max(13, 'Too Long!')
    .required('Required'),
    gender:Yup.string().required('Required'),
  });
class Register extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
    
    render() {
        
        return (
            <div style={customStyles.maindiv}>
            <Grid container>
             <Grid item xs={12} align="center">
               <Paper style={customStyles.paper}>
               <Grid item xs={12} align="center" style={customStyles.heading}>
                 Register With NeoSTORE
               </Grid>
               {/* <h1>Login to NeoSTORE</h1> */}
               <Formik
                 initialValues={{
                    first_name:'',
                    last_name:'',
                    email: '',
                    password:'',
                    cnfpassword:'',
                    gender:'',
                    phone:'',
                 }}
                 validationSchema={SignupSchema}
                 onSubmit={values => {
                    axios.post(`${BaseURL}/registerUser`, values)
                    .then(response => {
                        console.log(response.data)
                        if(response.data.success)
                        {
                          this.props.history.push('/login')
                        }
                        
                    })
                    .catch(error => {
                        console.log(error, 'error')
                    })
                   // same shape as initial values
                   console.log('values',values);
                 }}
               >
                 {({ errors, touched }) => (
                   <Form>
                       <Grid item xs={12} align="center">
                     <Field style={customStyles.textfield} placeholder="Enter First Name" name="first_name" type="text" />
                     {errors.first_name && touched.first_name ? <div style={customStyles.errordiv}>{errors.first_name}</div> : null}
                     </Grid>
                     <Grid item xs={12} align="center">
                     <Field style={customStyles.textfield} placeholder="Enter Last Name" name="last_name" type="text" />
                     {errors.last_name && touched.last_name ? <div style={customStyles.errordiv}>{errors.last_name}</div> : null}
                     </Grid>
                     
                     <Grid item xs={12} align="center">
                     <Field style={customStyles.textfield} placeholder="Email" name="email" type="email" />
                     {errors.email && touched.email ? <div style={customStyles.errordiv}>{errors.email}</div> : null}
                     </Grid>
                     <Grid item xs={12} align="center">
                     <Field style={customStyles.textfield} placeholder="Enter Password" name="password" type="password" />
                     {errors.password && touched.password ? <div style={customStyles.errordiv}>{errors.password}</div> : null}
                     </Grid>
                     <Grid item xs={12} align="center">
                     <Field style={customStyles.textfield} placeholder="Confirm Password" name="cnfpassword" type="password" />
                     {errors.cnfpassword && touched.cnfpassword ? <div style={customStyles.errordiv}>{errors.cnfpassword}</div> : null}
                     </Grid>
                     <Grid item xs={12} align="center">
                     <Field style={customStyles.textfield} placeholder="Enter Phone Number" name="phone" type="number" />
                     {errors.phone && touched.phone ? <div style={customStyles.errordiv}>{errors.phone}</div> : null}
                     </Grid>
                     <Grid item xs={12} align="center" style={customStyles.radiobtndiv}>
                        Gender
                        <Field
                          name="gender"
                          render={({ field }) => (
                            <div style={customStyles.radio}>
                              <div className="radio-item">
                                <input
                                  {...field}
                                  id="male"
                                  value="1"
                                  checked={field.value === '1'}
                                  name="gender"
                                  type="radio"
                                />
                                <label htmlFor="male">Male</label>
                              </div>
                              <div className="radio-item">
                                <input
                                  {...field}
                                    id="female"
                                    value="0"
                                    name="gender"
                                    checked={field.value === '0'}
                                    type="radio"
                                  />
                                  <label htmlFor="female">Female</label>
                                </div>
                            </div>        
                                )}
                                />
                                {errors.gender && touched.gender ? <div style={customStyles.errordiv}>{errors.gender}</div> : null}
                     </Grid>
                     <button style={customStyles.submitbtn} type="submit">Submit</button>
                   </Form>
                 )}
               </Formik>
               </Paper>
               </Grid>
           </Grid>
         </div>
        )
    }
}

export default Register
