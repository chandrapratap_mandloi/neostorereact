import React from 'react'
import SocialLogin from 'react-social-login'
// import { FacebookLoginButton } from "react-social-login-buttons";

const Button = ({ children, triggerLogin, ...props }) => (
    
  <button onClick={triggerLogin} {...props}>
      {console.log(props)}
    { children }
  </button>
)
 
export default SocialLogin(Button)