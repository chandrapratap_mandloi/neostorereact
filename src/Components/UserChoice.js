import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import PersonIcon from '@material-ui/icons/Person';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import AccountBoxIcon from '@material-ui/icons/AccountBox'
import ToggleOffIcon from '@material-ui/icons/ToggleOff'
import ToggleOnIcon from '@material-ui/icons/ToggleOn'
import HowToRegIcon from '@material-ui/icons/HowToReg'
// import ReorderIcon from '@material-ui/icons/Reorder'
import Isauth from './Isauth'
import { deleteCart } from './Redux/actions/cartActions';
import { connect } from 'react-redux';

function UserChoice(props) {
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);
  const authenticated = Isauth()
  // const [selectedIndex, setSelectedIndex] = React.useState(1);

  // function handleMenuItemClick(event, index) {
  //   setSelectedIndex(index);
  //   setOpen(false);
  // }

  function handleToggle() {
    setOpen(prevOpen => !prevOpen);
  }
  function userLogout (){
    // console.log('logout clicked')
    // localStorage.removeItem('user');
    // localStorage.removeItem('cartdata');
    // localStorage.removeItem('cartcount');
    // localStorage.removeItem('address');
    // props.history.push('/login')
    // props.deleteCart()
  }
  function UserLoggedIn() {
    console.log(props)
    return(
      <MenuList style={{width:170}}>
      {/* {options.map((option, index) => ( */}
        <MenuItem
          onClick={()=>props.history.push('/profile')}
        >
          <AccountBoxIcon />
          Profile
        </MenuItem>
        <MenuItem onClick={userLogout}><ToggleOffIcon/>Logout</MenuItem>
      {/* ))} */}
    </MenuList>
    )
  }

  function UserLoggedOut() {
    return(
      <MenuList style={{width:170}}>
        <MenuItem onClick={()=>props.history.push('/login')}> <ToggleOnIcon/> Login</MenuItem>
        <MenuItem onClick={()=>props.history.push('/register')}><HowToRegIcon/>Register</MenuItem>
      {/* ))} */}
    </MenuList>
    )
  }
  function handleClose(event) {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  }

  return (
    <Grid container>
      <Grid item xs={12} align="center">
        <ButtonGroup   ref={anchorRef} aria-label="split button">
          <Button
            style={{color:'white', width:100}}
            size="small"
            aria-owns={open ? 'menu-list-grow' : undefined}
            aria-haspopup="true"
            onClick={handleToggle}
          >
              <PersonIcon/>
            <ArrowDropDownIcon />
          </Button>
        </ButtonGroup>
        <Popper open={open} anchorEl={anchorRef.current} transition disablePortal>
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{
                transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
              }}
            >
              <Paper id="menu-list-grow">
                <ClickAwayListener onClickAway={handleClose}>
                    {authenticated ? UserLoggedIn()  :UserLoggedOut()}
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </Grid>
    </Grid>
  );
}

const mapStateToProps = state => ({
  cartQty: state.pReducer.cartQty
});

const mapDispatchToProps = dispatch => ({
  deleteCart: () => dispatch(deleteCart())
});

export default connect(mapStateToProps, mapDispatchToProps)(UserChoice)
// export default Address