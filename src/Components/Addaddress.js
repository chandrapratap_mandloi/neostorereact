import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import BaseURL from './Baseurl';
import axios from 'axios'
import Radio from '@material-ui/core/Radio';
import TextField from '@material-ui/core/TextField';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import '../CSS/user.css'
import Button from '@material-ui/core/Button';
import Decrypt from './Userdata'

  const customStyles={
      formdiv:{
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center'
      },
      maindiv:{
        display: 'flex',
        justifyContent: 'center',
        padding: '2% 2% 4% 2%'
      },
    textfield:{
      width:'30%',
      marginBottom:'2%',
      height:40,
      borderRadius:'4%',
      paddingLeft:'2%'
    },
    paper:{
      width:'85%',
      paddingTop:'2%',
      paddingBottom:'2%'
    },
    errordiv:{
      fontSize:12,
      color:'red'
    },
    submitbtn:{
      width: '54%',
      height: 30,
      backgroundColor:'#3f51b5',
      color:'white',
      border:'none'
  },
  radiodiv:{
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent:'center'
    },
    addressbtn:{ backgroundColor: '#3f51b5', color: 'white' },
    paperdiv:{width: '80%',
        paddingBottom: '2%'},
    themebtn:{marginRight: '5%'},
  }
class Addaddress extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            // error:true,
            userDetails: Decrypt().user,
            fields: {},
            errors: {},
            address:'',
            pincode:'',
            city:'',
            state:'',
            country:''
        }
    }
    handleValidation=()=>{
        let errors = {};
        var address = document.getElementById('address').value;
        var pincode = document.getElementById('pincode').value; 
        var city = document.getElementById('city').value;   
        var state = document.getElementById('state').value;   
        var country = document.getElementById('country').value;     
        let formIsValid = true;

        //Email
        if(address.length<10){
           formIsValid = false;
           errors["address"] = "Enter 10 Characters";
        }
        else if(pincode.length<6){
            formIsValid = false;
            errors["pincode"] = "Pincode must be of 6 digits";
         }
         else if(pincode.length>6){
            formIsValid = false;
            errors["pincode"] = "Only 6 Characters Allowed";
         }
         else if(city.length<3){
            formIsValid = false;
            errors["city"] = "City must be three characters long";
         }
         else if(state.length<3){
            formIsValid = false;
            errors["state"] = "State must be three characters long";
         }
         else if(country.length<3){
            formIsValid = false;
            errors["country"] = "Country must be three characters long";
         }
         else
         {
            //  console.log('else')
             formIsValid = true;
         }
        
        
       this.setState({errors: errors},()=> console.log(this.state.errors));
       return formIsValid;
   }
   handleSubmit = (event) => {
        event.preventDefault()
        const {userDetails} = this.state
        console.log('handle: ', this.handleValidation())
        const config = {
            headers: {
                'Authorization':`${userDetails.token}`
            }
        }
        const address={
            "address": this.state.address,
            "pincode": this.state.pincode,
            "city":this.state.city,
            "state":this.state.state,
            "country":this.state.country
        }
        console.log(address)
        if(this.handleValidation()){
            axios.post(`${BaseURL}/address`,address,config)
            .then(response =>{
                if(response.data.success)
                {
                    console.log(response.data)
                    this.props.cbFn(false)
                }
            })
            .catch(error => {console.log(error,'error')})
        }
        else{
            alert("Form has errors")
        }
    }
    handlechange=(event,type)=>{
        console.log(event,type)
        if(type==='address')
        {
            this.setState({address:event.target.value})
        }
        else if(type==='pincode')
        {
            this.setState({pincode:event.target.value})
        }
        else if(type==='city')
        {
            this.setState({city:event.target.value})
        }
        else if(type==='state')
        {
            this.setState({state:event.target.value})
        }
        else if(type==='country')
        {
            this.setState({country:event.target.value})
        }
    }
    render() {
        console.log(this.props)
        return (
            <div style={customStyles.maindiv}>
                {/* <Paper style={customStyles.paperdiv}> */}
                    <Grid container>
                    <form style={customStyles.formdiv} onSubmit={this.handleSubmit}> 
                        <Grid item xs={12} align="center">
                            <TextField
                                required
                                id="address"
                                label="Address"
                                autoComplete='address-line1'
                                className={customStyles.textfield}
                                onChange={()=>this.handlechange(event,'address')}
                                margin="normal"
                            />
                            <br/>
                            <span style={{color: "red",fontSize:12}}>
                                <b>{this.state.errors["address"]}</b>
                            </span>
                        </Grid>
                        <Grid item xs={12} align="center">
                            <TextField
                                required
                                id="pincode"
                                label="Pincode"
                                type="number"
                                autoComplete='postal-code'
                                className={customStyles.textfield}
                                onChange={()=>this.handlechange(event,'pincode')}
                                margin="normal"
                            />
                            <br/>
                            <span style={{color: "red",fontSize:12}}>
                                <b>{this.state.errors["pincode"]}</b>
                            </span>
                        </Grid>
                        {/* <Grid container> */}
                            <Grid item xs={12} align="center">
                                <TextField
                                    required
                                    id="city"
                                    label="City"
                                    autoComplete='address-level2'
                                    className={customStyles.textfield}
                                    onChange={()=>this.handlechange(event,'city')}
                                    margin="normal"
                                />
                                <br/>
                                <span style={{color: "red",fontSize:12}}>
                                    <b>{this.state.errors["city"]}</b>
                                </span>
                            </Grid>
                            <Grid item xs={12} align="center">
                                <TextField
                                    required
                                    id="state"
                                    label="State"
                                    autoComplete='address-level1'
                                    className={customStyles.textfield}
                                    onChange={()=>this.handlechange(event,'state')}
                                    margin="normal"
                                />
                                <br/>
                                <span style={{color: "red",fontSize:12}}>
                                    <b>{this.state.errors["state"]}</b>
                                </span>
                            </Grid>
                        {/* </Grid> */}
                        <Grid item xs={12} align="center">
                            <TextField
                                required
                                id="country"
                                label="Country"
                                autoComplete='country-name'
                                className={customStyles.textfield}
                                onChange={()=>this.handlechange(event,'country')}
                                margin="normal"
                            />
                            <br/>
                            <span style={{color: "red",fontSize:12}}>
                                <b>{this.state.errors["country"]}</b>
                            </span>
                        </Grid>
                        <Grid item xs={12} >
                            <Grid item style={{paddingLeft:'40%'}} xs={12}>
                                Address Type:
                            </Grid>
                            {/* <br/> */}
                            <RadioGroup label="Home" aria-label="Address Type" name="gender2" onChange={this.handleGenderChange} style={customStyles.radiodiv}>
                                <FormControlLabel
                                    value="0"
                                    control={<Radio color="primary" />}
                                    label="Home"
                                    labelPlacement="start"
                                />
                                <FormControlLabel
                                    value="1"
                                    control={<Radio color="primary" />}
                                    label="Office"
                                    labelPlacement="start"
                                />
                            </RadioGroup> 
                            <br/>
                        </Grid>
                        <Grid item xs={12} align="center">
                            <Button type='submit' 
                                style={customStyles.themebtn} 
                                variant="outlined" 
                                color="primary"
                            >
                                Add Address
                            </Button>
                            <Button type='button' 
                                onClick={()=>this.props.cbFn(false)} 
                                variant="outlined" 
                                color="primary" 
                                style={customStyles.themebtn}
                            >
                               Cancel 
                            </Button>
                        </Grid>
                        </form>
                    </Grid>
                {/* </Paper> */}
            </div>
        )
    }
}

export default Addaddress
