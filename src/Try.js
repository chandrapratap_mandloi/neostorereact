import React, { Component } from 'react'
import BaseURL from './Components/Baseurl'
import axios from 'axios' 
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Rating from '@material-ui/lab/Rating';
import Button from '@material-ui/core/Button';
// import CustomizedExpansionPanels from './Accordian'
import AccordionSide from './Components/Accordion'

const customStyles = {
    subdiv: {
      paddingTop:'1%',
      paddingLeft:'2%',
      paddingRight:'2%',
      paddingBottom:'1%'
    },
    img:{
        height: 150,
        width: '100%',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 5,
        alignItem:'center'
    },
    card:{
        maxWidth:450,
        height:325, 
        paddingTop:'1%', 
        paddingLeft:'1%', 
        paddingRight:'1%', 
        paddingBottom:'1%'
    },
    imgcard:{
        display:'flex', 
        justifyContent:'center',
        marginBottom:'2%',
        // marginRight:'1%'
    },
    maindiv:{
        display:'flex',
        flexWrap:'wrap',
        justifyContent:'center'
    }
  };
class Try extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             data:[],
        }
    }
    componentDidMount() {
        console.log(this.props)
        if(this.props.match.params.id===undefined){
            console.log('inside if')
            axios.get(`${BaseURL}/findproducts`)
            .then(response => {
                console.log(response.data)
                this.setState({
                    data:response.data.product
                })

            })
            .catch(error => { console.log(error, 'error') })
        }
        else{
            console.log('inside else')
            axios.get(`${BaseURL}/findproducts?category_id=${this.props.match.params.id}`)
            .then(response => {
                console.log(response.data)
                this.setState({
                    data:response.data.product
                })
            })
            .catch(error => { console.log(error, 'error') })
        }
        
    }
    showProduct(step){
        console.log(step,'clicked card')
        this.props.history.push({
            pathname:`product/${step._id}`,
            state: {step}
          }) 
    }

    render() {
        const {data} = this.state
        return data.length>0 && (
            <div>
                <Grid style={customStyles.subdiv} container spacing={1}>
                <Grid item xs={2} column>
                    <AccordionSide {...this.props}/>
                </Grid>
                <Grid item xs={10} style={customStyles.maindiv} column>
                 {data.map((step, index) =>  
                 <Grid item xs={4} sm={4} md={4} style={customStyles.imgcard}>
                <Card onClick={()=>this.showProduct(step)}  key={step._id} style={customStyles.card}>
                    <CardActionArea  key={step.index}>
                        <CardMedia
                        component="img"
                        alt="Contemplative Reptile"
                        style={customStyles.img}
                        image={`${BaseURL}/${step.product_image[0]}`}
                        title="Contemplative Reptile"
                        />
                        <CardContent style={{textAlign:'center'}} >
                        <Typography gutterBottom variant="h5" component="h2">
                            {step.product_name}
                        </Typography>
                        <Typography gutterBottom variant="h5" component="h2">
                            {step.product_cost}
                        </Typography>
                        {/* <div> */}
                           <Button style={{backgroundColor:'#3f51b5',color:'white'}}>
                               Add To Cart
                           </Button>
                        {/* </div> */}
                        <Typography gutterBottom variant="h5" component="h2">
                            <Rating name="half-rating" value={step.product_rating} precision={0.5} readOnly/>
                        </Typography>
                        </CardContent>
                    </CardActionArea>
                    </Card>
                    </Grid>
                )}
                </Grid>
                </Grid>
            </div>
        )
    }
}

export default Try
