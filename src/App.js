// import React, { Component,lazy, Suspense  } from 'react';
import React, { Component, Suspense  } from 'react';
import './App.css';
import ProductDesc from './Components/ProductDesc'
import { Route,HashRouter, Switch, Redirect } from 'react-router-dom'
import AllProduct from './Components/AllProduct'
import IntegrationAutosuggest from './Components/IntegrationAutosuggest'
import Header from './Components/Header'
import Footer from './Components/Footer'
import Login from './Components/Login'
import Register from './Components/Register'
import Dashboard from './Components/Dashboard'
import Social from './Components/Social'
import UserDrawer from './Components/UserDrawer'
import ReduxDemo from './Components/ReduxDemo'
// import { withRouter } from 'react-router-dom'
import ShowCart from './Components/ShowCart'
// import CartEmpty from './Components/CartEmpty'
// import IconTabs from './Components/CartTab'
import OrderPlaced from './Components/OrderPlaced'
import Pdf from './Components/Pdf'
import Address from './Components/Address'
import PrivateRoute from './Components/PrivateRoute'
import Notfound from './Components/Notfound'
import Functional from './Components/Functional'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return(
      <Suspense fallback={<h1>Still Loading…</h1>}>
      <HashRouter>
  <Header {...this.props}/>
    <Switch>
   
          <Route exact path="/dashboard" component={Dashboard} />
          <Route exact path="/product/:id?" component={ProductDesc} />
          <Route exact path="/productpage/:id?/:color_id?" component={AllProduct}/>
          <Route exact path="/try" component={ReduxDemo}/>
          <Route exact path="/tryy" component={Functional}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/register" component={Register}/>
          <Route exact path="/social" component={Social}/>
          <Route exact path="/profile" component={UserDrawer}/>
          <Route exact path="/search" component={IntegrationAutosuggest}/>
          <Route exact path="/cart" component={ShowCart}/>
          <PrivateRoute exact path="/address" component={Address}/>
          <Route exact path="/pdf" component={Pdf}/>
          <Route exact path="/orderplaced" component={OrderPlaced}/>
          <Redirect exact from="/" to="dashboard" />
          <Route component={Notfound} />
      </Switch>
  <Footer/>
    </HashRouter>
    </Suspense>
    )
  }
}
export default App
// export default withRouter(App);
